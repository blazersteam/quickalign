<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'livequic_newdb');

/** MySQL database username */
define('DB_USER', 'livequic_usrq');

/** MySQL database password */
define('DB_PASSWORD', 'Y(Z~m0QGz[%F');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B(a;;KDip2{i#k%S;|fp~,v/.GzIvfI{l(?6sR0!V(f@UMf75/q(|ge2[o}l}W`|');
define('SECURE_AUTH_KEY',  'uHWf:qKA* ez3Etl|7PGjv}R)*;zq6UV]Yx6Kt]m}&xQDj7K}DNqDSV5cvcXU$2u');
define('LOGGED_IN_KEY',    '{)sv/lT&aME{i+o#m14{RY9kEKkCOyysx&`_W{jrVi`!b=rrjt :pmHT?0aaoZyl');
define('NONCE_KEY',        '+|z/P>({c2UVBj[3s#02`]d:Dl6_zQ70Gp[Kw}[Jpec6KWlI0cAfGQ>G:c;uti?s');
define('AUTH_SALT',        'I,_LK3=|Q.l PSP)F_Huk+E0T.E(A<PZw+,ge9d`(0Lz2f--iQ0Iu73 dJ@(<ze*');
define('SECURE_AUTH_SALT', 'u?CG3jI2PcS[A}0a!kE,eqA+2[MRz;]?Fokiz5;)1>G0<I.x}J%_JOO%41Wn<In{');
define('LOGGED_IN_SALT',   '@_mJ??o#1d|_+49L8!,Lu:@[)RhgydfHFWp:C@Wyf. 7I8t9$oK8(QHJGH&yhP`h');
define('NONCE_SALT',       'bx:BgW DVQC3iPYyd;KHI}^foV7#9ZVMy]l+,,xk`.q4[o;`(;?Zv:B[t4wSHnU#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_MEMORY_LIMIT' , '256M' );
define( 'WP_AUTO_UPDATE_CORE' , false );
define( 'DISALLOW_FILE_EDIT', true );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
