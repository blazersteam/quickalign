e��]<?php exit; ?>a:1:{s:7:"content";s:246494:"a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:58:"
	
	
	
	

















































";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"WordPress Planet";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:28:"http://planet.wordpress.org/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:2:"en";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:47:"WordPress Planet - http://planet.wordpress.org/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:47:{i:0;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"WPTavern: The Power of Stories: Chris Lema and the Bridge Framework";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=95240";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:77:"https://wptavern.com/the-power-of-stories-chris-lema-and-the-bridge-framework";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5474:"<p>I would tell you that when Chris Lema, VP of Products at Liquid Web, is speaking, you should listen.  But, there is no need to say that.  He has an infectious quality that grabs your attention and doesn&#8217;t let go.  I found myself hanging onto every word in his session, <a href="https://2019.us.wordcamp.org/session/the-content-framework-that-powers-stories-landing-pages-more/">The Content Framework that Powers Stories, Landing Pages, &amp; More</a>, via the WordCamp U.S. livestream this past weekend.</p>



<p>Telling stories is a uniquely human trait.  Our ability to weave narratives together is what separates us from lower animals.  Sure, other important things such as the ability to make fire, understand advanced mathematics, and build rocket-powered ships all set us apart.  But, it is the stories we tell that are the most interesting things about us as a species.</p>



<p>Any good story leaves you waiting to see what will happen next and how the people within those stories react.  How they grow.  How they change.</p>



<p>This fundamental human activity was at the heart of Lema&#8217;s 15-minute presentation.  &#8220;When people believe that you&#8217;ve been where they are and can see that you&#8217;ve gotten to the other side, they will follow,&#8221; said Lema of selling products.</p>



<p>Ultimately, the bridge framework is about guiding others through your journey and helping them cross the bridge you have found.  This framework can apply to your brand, your products, or any other content that you are providing to others.</p>



<p>One thing product makers often fail at is providing a solution before sharing how they have encountered the same problem.  &#8220;No one feels like they need a bridge until they are facing a river,&#8221; said Lema.  The struggle must come first.</p>



<h2>What Comes After the Product</h2>



<p>In 2007, I built one of the most popular themes ever in WordPress&#8217; short history.  It does not matter what theme it was.  It is long retired.  What mattered was it helped users get to their destination.</p>



<p>One theme user who stood out was building a Formula 1 racing website.  I was a mediocre designer at best, but this user would create some of the most beautiful customizations that I had ever seen.  It seemed like he would change the design every week.  Each time, I was in awe at his talent.  He continued using this same theme of mine for years, even after I archived it and moved onto other theme projects.</p>



<p>What I should have learned during those years was, without knowing, I had the story right.  I knew the technical aspects of why this specific theme was a leap forward.  However, I didn&#8217;t understand the story I was telling users was drawing people in.</p>



<p>I had been where they were.  I had struggled to get to where I was going.  I had braved the journey beyond that point and found a path for others to join me.</p>



<p>As time moved on, I became a better developer.  I had one more insanely popular theme.  Again, it was about the story.  I could recognize the problems.  I had the same frustrations as others.  I had a way to fix those problems and get people from Point A to Point B.  I invited others along.  I told them I would be there every step of the way.</p>



<p>I never recreated that early success with another theme, at least not on the same scale.</p>



<p>I stopped focusing on what mattered.</p>



<p>I marketed future themes based far too much on the technical aspects.  Essentially, I was flaunting my development skills.  After years of lucking into success by being a storyteller, I tried to follow the trends of others who were marketing their HTML5, CSS3, or whatever other keyword was popular at the time.</p>



<p>Fortunately, I had loyal users who stuck with me over the years.  There was one theme user who would often switch themes whenever I released a new one.  Like the racing enthusiast, this person would put his own spin on the design.  He used the themes on his photography site.  What was interesting about some of the themes was they were not specifically built with photography in mind.  That was never my goal when creating them.</p>



<p><em>What was it that made this user continue using different themes of mine?</em></p>



<p>It was never about all the bells and whistles.  Many of them were unused on the site.  It was about what came after activating the theme.  It wasn&#8217;t about me.  It was about the user being able to tell his own story through photos.</p>



<p>In hindsight, I could see that the projects I achieved the most success with were the projects I was the most passionate about.  I had built them to solve specific problems.  The technical details did not matter.  I had built or found a bridge to get to the place that I wanted to be.  My excitement and passion naturally transferred to how I spoke about those projects.  It changed how I sold them to users.  I told my story.</p>



<p>The biggest failures I had were when I did not have a good story to tell.</p>



<h2>Watch Chris Lema&#8217;s Speech</h2>



<p>For those that are running any type of business, you owe it to yourselves to listen to Lema explain how to connect with customers.</p>



<div class="wp-block-embed__wrapper">

</div>



<p><em>Lema&#8217;s session starts at the 2:59:46 mark if the videos doesn&#8217;t start at the correct point. The embedded video should begin at his introduction.</em></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 06 Nov 2019 17:53:14 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:28:"Matt: State of the Word 2019";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"https://ma.tt/?p=50414";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:45:"https://ma.tt/2019/11/state-of-the-word-2019/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2061:"<p>In case you missed it, here&#8217;s the first-ever State of the Word&#8230; designed completely in Gutenberg: </p>



<div class="wp-block-embed__wrapper">

</div>



<p><a href="https://2019.us.wordcamp.org/">WordCamp US</a> was a fantastic experience, as always. Thank you again to the hundreds of organizers and volunteers who made it happen, to the thousands who attended, and to the city of St. Louis for hosting us. We&#8217;ll be back there again next year.</p>



<p>And special thanks to this next generation of WordPress contributors. So exciting to see <a href="https://2019.us.wordcamp.org/kidscamp/">KidsCamps</a> continue to expand and thrive:</p>



<div class="wp-block-embed__wrapper">
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Thank you <a href="https://twitter.com/photomatt?ref_src=twsrc%5Etfw">@photomatt</a> for stopping by KidsCamp today. The kids loved your visit! <a href="https://twitter.com/hashtag/WordPress?src=hash&ref_src=twsrc%5Etfw">#WordPress</a> <a href="https://twitter.com/hashtag/KidsCamp?src=hash&ref_src=twsrc%5Etfw">#KidsCamp</a> <a href="https://twitter.com/hashtag/WCUS?src=hash&ref_src=twsrc%5Etfw">#WCUS</a> <a href="https://t.co/cq65sHkjsI">pic.twitter.com/cq65sHkjsI</a></p>&mdash; Sandy Edwards (@sunsanddesign) <a href="https://twitter.com/sunsanddesign/status/1191093861172559873?ref_src=twsrc%5Etfw">November 3, 2019</a></blockquote>
</div>



<p>As you can see, my site is now featuring the new <a href="https://make.wordpress.org/core/2019/09/06/introducing-twenty-twenty/">WordPress Twenty Twenty theme</a>. And for more coverage from my State of the Word, check out the recaps from <a href="https://wptavern.com/state-of-the-word-2019-recap-all-roads-lead-to-the-block-editor">WP Tavern</a> and <a href="https://poststatus.com/matt-mullenweg-state-of-the-word-2019/">Post Status</a>. Here&#8217;s my full audience Q&amp;A below: </p>



<div class="wp-block-embed__wrapper">

</div>



<p>You can see my previous <a href="https://ma.tt/tag/sotw/">State of the Word keynotes here</a>. </p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 06 Nov 2019 06:38:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"WordPress.org blog: WordPress 5.3 RC4";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7596";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wordpress.org/news/2019/11/wordpress-5-3-rc4/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3672:"<p>The fourth release candidate for WordPress 5.3 is now available!</p>



<p>WordPress 5.3 is currently scheduled to be released on&nbsp;<strong><a href="https://make.wordpress.org/core/5-3/">November 12 2019</a></strong>, but we need&nbsp;<em>your</em>&nbsp;help to get there—if you haven’t tried 5.3 yet, now is the time!</p>



<p>There are two ways to test the WordPress 5.3 release candidate:</p>



<ul><li>Try the&nbsp;<a href="https://wordpress.org/plugins/wordpress-beta-tester/">WordPress Beta Tester</a>&nbsp;plugin (choose the “bleeding edge nightlies” option)</li><li>Or&nbsp;<a href="https://wordpress.org/wordpress-5.3-RC4.zip">download the release candidate here</a>&nbsp;(zip).</li></ul>



<p>For details about what to expect in WordPress 5.3, please see the&nbsp;<a href="https://wordpress.org/news/2019/10/wordpress-5-3-release-candidate/">first</a>, &nbsp;<a href="https://wordpress.org/news/2019/10/wordpress-5-3-rc2/">second</a>&nbsp;and <a href="https://wordpress.org/news/2019/10/wordpress-5-3-rc3/">third</a> release candidate posts.</p>



<p>Release Candidate 4 contains three bug fixes for the new default theme, Twenty Twenty (see <a href="https://core.trac.wordpress.org/ticket/48450">#48450</a>), and addresses the following:</p>



<ul><li>The Twemoji library has been updated from 12.1.2 to 12.1.3 (see <a href="https://core.trac.wordpress.org/ticket/48293">#48293</a>).</li><li>Two regressions in the Media component (see <a href="https://core.trac.wordpress.org/ticket/48451">#48451</a> and <a href="https://core.trac.wordpress.org/ticket/48453">#48453</a>).</li><li>One bug in the Upload component (see <a href="https://core.trac.wordpress.org/ticket/48472">#48472</a>)</li><li>Five bugs in the Block Editor component (see <a href="https://core.trac.wordpress.org/ticket/48502">#48502</a>)</li></ul>



<h2>Plugin and Theme Developers</h2>



<p>Please test your plugins and themes against WordPress 5.3 and update the&nbsp;<em>Tested up to</em>&nbsp;version in the readme to 5.3. If you find compatibility problems, please be sure to post to the&nbsp;<a href="https://wordpress.org/support/forum/alphabeta/">support forums</a>&nbsp;so we can figure those out before the final release.</p>



<p>The&nbsp;<a href="https://make.wordpress.org/core/2019/10/17/wordpress-5-3-field-guide/">WordPress 5.3 Field Guide</a>&nbsp;has also been published, which details the major changes.</p>



<p>A new dev note has been published since the Field Guide was released, <a href="https://make.wordpress.org/core/2019/11/05/use-of-the-wp_update_attachment_metadata-filter-as-upload-is-complete-hook/">Use of the “wp_update_attachment_metadata” filter as “upload is complete” hook</a>. Plugin and theme authors are asked to please read this note and make any necessary adjustments to continue working well in WordPress 5.3 or share any difficulties encountered on&nbsp;<a href="https://core.trac.wordpress.org/ticket/48451">#48451</a>.</p>



<h2>How to Help</h2>



<p>Do you speak a language other than English?&nbsp;<a href="https://translate.wordpress.org/projects/wp/dev">Help us translate WordPress into more than 100 languages!</a></p>



<p><em><strong>If you think you’ve found a bug</strong>, you can post to the&nbsp;<a href="https://wordpress.org/support/forum/alphabeta">Alpha/Beta area</a>&nbsp;in the support forums. We’d love to hear from you! If you’re comfortable writing a reproducible bug report,&nbsp;<a href="https://make.wordpress.org/core/reports/">file one on WordPress Trac</a>, where you can also find&nbsp;<a href="https://core.trac.wordpress.org/tickets/major">a list of known bugs</a>.</em></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 05 Nov 2019 23:56:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Francesca Marano";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:79:"BuddyPress: Test BuddyPress 5.1.0-beta1 thanks to our new BP Beta Tester plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=308797";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://buddypress.org/2019/11/buddypress-5-1-0-beta1/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2816:"<p>Hi BuddyPress contributors!</p>



<p>We will soon publish a maintenance release (<strong>5.1.0</strong>) to fix some issues that arose since BuddyPress 5.0.0 “Le Gusto”. A detailed changelog will be part of our official release notes, but, until then, you can check out this <a href="https://buddypress.trac.wordpress.org/query?status=closed&group=resolution&milestone=5.1.0">report on Trac</a> for the full list of fixes.</p>



<p>Today we&#8217;re publishing a very specific<a href="https://downloads.wordpress.org/plugin/buddypress.5.1.0-beta1.zip"> beta release</a> for version 5.1.0 as it has two goals:</p>



<ul><li>Let you make sure the fixes have no side effects on your community site&#8217;s configuration.</li><li>Test in real conditions the plugin we&#8217;ve been working on and which should greatly simplify the way you betatest BuddyPress.</li></ul>



<h2>Meet BP Beta Tester</h2>



<img src="https://buddypress.org/wp-content/uploads/1/2019/11/bp-beta-tester-github-featured-image-1024x512.jpeg" alt="" class="wp-image-308803" />



<p>Once installed it will help you to upgrade your website to the latest Beta or Release candidate. You will also be able to downgrade to the latest stable release once you finished your Beta tests.</p>



<img src="https://buddypress.org/wp-content/uploads/1/2019/11/bp-beta-tester-01.png" alt="" class="wp-image-308800" />



<p>Once activated, go to the home page of your Dashboard (Network Dashboard if your are using WordPress Multisite) to find the BP Beta Tester sub menu of the Dashboard menu. From this page, you&#8217;ll be able to install the 5.1.0-beta1 release clicking on the tab &#8220;Upgrade to 5.1.0-beta1&#8221;.</p>



<img src="https://buddypress.org/wp-content/uploads/1/2019/11/bp-beta-tester-02.png" alt="" class="wp-image-308801" />You can always downgrade to the latest stable version of BuddyPress using the corresponding tab of the page’s header.



<p>The development version of this plugin is hosted on <a href="https://github.com/buddypress/bp-beta-tester">GitHub</a>: you can contribute to it <a href="https://github.com/buddypress/bp-beta-tester/pulls">pulling requests</a> or <a href="https://github.com/buddypress/bp-beta-tester/issues">reporting issues</a>. We plan to submit this plugin on the WordPress.org Plugins directory so that it’s easier to install.</p>



<p><a href="https://github.com/buddypress/bp-beta-tester/releases/download/1.0.0-beta1/bp-beta-tester.zip"><strong>Download BP Beta Tester</strong></a><strong> or </strong><a href="https://downloads.wordpress.org/plugin/buddypress.5.1.0-beta1.zip"><strong>Download BuddyPress 5.1.0-beta1</strong></a><strong>.</strong></p>



<p>Happy testing <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f609.png" alt="😉" class="wp-smiley" /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 05 Nov 2019 14:48:52 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"imath";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:74:"WPTavern: State of the Word 2019 Recap: All Roads Lead to the Block Editor";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=95216";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://wptavern.com/state-of-the-word-2019-recap-all-roads-lead-to-the-block-editor";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:11947:"<div class="wp-block-image"><img /></div>



<p>If there was one common theme in Matt Mullenweg&#8217;s State of the Word address this year at WordCamp U.S., it was that all roads lead to the block editor.  His speech was primarily about blocks, blocks, more blocks, and a dash of community.  This doesn&#8217;t come as a surprise because we are closing in on the one year mark of the official merge of the Gutenberg plugin into the core WordPress code.  It has been a year about blocks, and nothing is changing that course.</p>



<p><a href="https://2019.us.wordcamp.org">WordCamp U.S. 2019</a> was held in St. Louis, Missouri, over this past weekend.  The event was planned and put together by 47 organizers and 122 volunteers.  There were 90 speakers who held sessions across a range of topics in multiple rooms.</p>



<p>For people who were not able to attend or watch via the livestream, the sessions are <a href="https://www.youtube.com/channel/UCpJf6LGZ0a4n9Lj4aVt9spg">available via YouTube</a>.  Eventually, the videos will also make their way over to <a href="https://wordpress.tv/event/wordcamp-us-2019/">WordPress.tv</a></p>



<h2>Open: The Community Code</h2>



<p>Mullenweg opened The State of the Word by showing a documentary named <a href="https://open.film/">Open: The Community Code</a>, which is a film that primarily focuses on the WordPress community.</p>



<p>The film explores why people are so passionate about a project that is essentially just code.  What drives them to organize and attend events like WordCamps?  Why do they volunteer their free time contributing to an industry that sees over $10 billion in profits?  What makes the WordPress community different from other projects?  The film team interviewed 37 people to get to the bottom of these questions.</p>



<p>The team behind the project is also providing the film and all of the raw footage as open source for others to use.</p>



<div class="wp-block-embed__wrapper">
<div class="embed-vimeo"></div>
</div>



<h2>The Events of the Past Year</h2>



<p>Mullenweg primarily focused on WordPress updates and changes within the community when recapping events of the past year.  Since the <a href="https://wptavern.com/wordpress-5-0-bebo-released-lays-a-foundation-for-the-platforms-future">release of WordPress 5.0</a> on December 6, 2018, WordPress has had two major releases.  A third major release, WordPress 5.3, is scheduled to launch on November 12.  </p>



<p>During 2019, most heavy work went into the Gutenberg plugin, which was ported back into core WordPress.  The number of contributors to Gutenberg more than doubled since WordPress 5.0 launch, an increase from 200 to 480 individuals.</p>



<p>The <a href="https://wptavern.com/wordpress-5-1-improves-editor-performance-encourages-users-to-update-outdated-php-versions">release of WordPress 5.1</a> introduced the first iteration of the site health page, new cron features, and a site meta table for multisite installations.</p>



<p>&#8220;WordPress is all about empowering users and we wanted to put the information and the tools in the hands of users as well to keep the site running in tip-top shape as we power an ever-increasing percentage of the web,&#8221; said Mullenweg of the site health feature.  He further explained that it is WordPress&#8217; responsibility to make sure users are up to date and running the latest versions of software.</p>



<p>Building on top of the site health introduction, <a href="https://wptavern.com/wordpress-5-2-jaco-released-includes-fatal-php-error-protection-and-a-recovery-mode">WordPress 5.2 launched</a> with a PHP fatal error protection and recovery mode.  The release also bumped the minimum PHP version to 5.6 and ported all widgets to blocks.</p>



<p>Mullenweg then outlined the work done toward getting WordPress 5.3 ready for its November 12 launch date.  The major changes include:</p>



<ul><li>150+ block editor improvements</li><li>Twenty Twenty default theme</li><li>Date/Time improvements and fixes</li><li>PHP 7.4 compatibility</li></ul>



<p>As of now, 83% of all users on WordPress 5.2 or newer are running at least PHP 7.  This means the WordPress project has done what it can from the user end.  It is now time to start working with hosts to get sites updated to the latest version of PHP.</p>



<p>The block editor is now available on both Android and iOS devices.  Mullenweg announced they were almost done with offline post support and that a dark mode is coming in weeks.</p>



<p>The community had a good year.  In 2019, there were 141 WordCamp events, 34 of which were in new cities.  There were 17 Kids Camps for younger contributors to get involved.  There were also over 5,000 meetups and 16 <a href="https://doaction.org/">do_action() charity hackathons</a>.</p>



<p>The <a href="https://wordpress.org/news">WordPress news page</a> has been highlighting one story from <a href="https://heropress.com/">HeroPress</a> every month in the past year.  HeroPress is a project that allows people to tell their stories of how they got involved with WordPress.</p>



<p>Mullenweg held a moment of silence for long-time community member Alex Mills (viper007bond) who passed away earlier this year after a <a href="https://alex.blog/2019/02/18/leukemia-has-won/">long-fought battle</a> with leukemia.  Automattic is planning to finance a scholarship in his honor.  The scholarship will go to a plugin developer to attend WordCamp U.S. who has not had an opportunity to attend.</p>



<h2>2019: The Year of the Block Editor</h2>



<div class="wp-block-image"><img />Slide with screenshots of Gutenberg criticism from users.</div>



<p>Mullenweg started focusing on the block editor after recapping the events of the past year.  WordPress 5.0 was released one day before WordCamp U.S. 2018 in Nashville.</p>



<p>&#8220;We had people coordinating work from airplanes,&#8221; said Mullenweg.  &#8220;There were impromptu groups of core developers, testing and packaging the release in the hallways. The polyglots, marketers, and support teams were just scrambling to get ready.&#8221;</p>



<p>He explained the reason for the biggest change to WordPress in its then 16-year history.  &#8220;We came together and decided to make this big change cause we wanted to first disrupt ourselves. We wanted to empower more WordPress users to realize our mission of democratizing publishing, and wanted to make the web a more open and welcoming place.&#8221;</p>



<p>Not everyone was happy with the direction of WordPress and its new block editor.  It was a rough year from a leadership perspective to have a vision and see it through, despite constant negative feedback.  Mullenweg highlighted some of the comments that were critical of the block editor and explained that they had learned a lot from the process.</p>



<p>&#8220;I think that we also have a great opportunity when we make big changes in the future,&#8221; said Mullenweg.  &#8220;Sort of build that trust in the conversations around testing, using GitHub for development, things like accessibility. So, I understand why we had a lot of this feedback.  But, we did get through it together.&#8221;</p>



<p>Mullenweg highlighted that, according to Jetpack plugin stats, over 50 million posts have been written in the block editor.  That amounts to around 270 thousand posts per day.  It is important to note that this stat is on the lower end because it only accounts for users of the Jetpack plugin.  Therefore, the number is likely much higher.</p>



<p>He covered the performance improvements to the editor, block motion when moving blocks, typewriter mode, block previews, and the social block.  &#8220;These are like the Nascar stickers of the web,&#8221; he said of social icons.  &#8220;They&#8217;re everywhere.&#8221;</p>



<h2>The Next Steps for the Block Editor</h2>



<p>In his address, Mullenweg covered the four phases of the Gutenberg project.</p>



<ol><li>Easier Editing</li><li>Customization</li><li>Collaboration</li><li>Multilingual</li></ol>



<p>The first phase was the initial launch and iteration of the block editor for content.  The second stage, which we are in now, is about full site customization.  This includes widgets and non-content areas, and will eventually cover areas like the site header and footer.  It will be interesting to see how page-building plugins work with these upcoming changes.  Some could use WordPress as the foundational, framework-type layer.  Others may go their own way.  Themes will also have to keep pace with the changes.</p>



<p>Phase three, collaboration, will introduce a feature that allows multiple authors to collaborate and co-edit posts on a site in real time.  With any luck, WordPress will also build in a proper system for attributing posts to multiple authors.</p>



<p>The fourth and final phase cannot get here fast enough.  As WordPress usage continues to grow around the world, it is past time that it offered a multilingual experience.  &#8220;We&#8217;re going to tackle the Babel fish problem,&#8221; said Mullenweg.</p>



<p>Also on the roadmap is the concept of block patterns.  Patterns would be a groups of blocks that follows common patterns seen across the web.  The existing Media &amp; Text block is an example of a common pattern, but new patterns would go far beyond something so basic.  By providing patterns to users, they could simply insert a pattern and fill in their details, which should make it easy to rapidly create rich content.</p>



<h2>Watch the State of the Word</h2>



<div class="wp-block-embed__wrapper">
<div class="embed-wrap"></div>
</div>



<p>Mullenweg&#8217;s entire presentation was done from the block editor.  He used the <a href="https://wordpress.org/plugins/slide/">Slides plugin</a> created by Ella van Durpe.</p>



<h2>Community Questions and Answers</h2>



<p>The Q&amp;A sessions after Mullenweg&#8217;s address was more focused on community and policy.</p>



<p>Rian Kinney asked whether we would see official policies on accessibility, ethics, conflicts of interest, and diversity.  She wanted to know how the community could make this happen over the next year.</p>



<p>While a privacy policy is in the footer of WordPress.org, Mullenweg expressed his desire to not make changes than lean too heavily on policy.  &#8220;That is in spite of there being a policy or not, we&#8217;ve tried to enact bigger changes in WordPress in a policy-first way in the past,&#8221; he said.  &#8220;To be honest, it felt nice but didn&#8217;t always make things actually change.&#8221;  He said we usually do better by working with people to make changes rather than starting with the policy.</p>



<p>Olivia Bisset, a young WordCamp speaker behind <a href="http://lemonadecode.com">Lemonade Code</a>, asked Mullenweg how we could inspire kids who are currently in school to get involved with WordPress.  The project has tough competition coming from more exciting technology sectors such as robotics and other industries that are swaying the next generation.</p>



<p>&#8220;This is going to be on YouTube later, and boys and girls, maybe of your generation, will see you here asking a question and being a speaker at WordCamp in front of a thousand adults,&#8221; said Mullenweg.  &#8220;And, you know, it&#8217;s kind of beautiful.&#8221;</p>



<p>Mullenweg said that we need more stories from younger people on HeroPress and that Kids Camps will help.  He said that WordPress should be easier and more accessible, which are things that the current generation is more aware of and care about.  He also mentioned Automattic&#8217;s recent acquisition of Tumblr, which has a larger user base of young users, as a way to introduce them to WordPress.</p>



<p>View the Q&amp;A portion of The State of the Word in the following video.</p>



<div class="wp-block-embed__wrapper">
<div class="embed-wrap"></div>
</div>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 04 Nov 2019 19:18:46 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:55:"Post Status: Matt Mullenweg’s State of the Word, 2019";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=70832";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:61:"https://poststatus.com/matt-mullenweg-state-of-the-word-2019/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:9653:"<p><a href="https://2019.us.wordcamp.org/">WordCamp US 2019</a> completes its first year in St. Louis, Missouri, where more than 1,300 WordPress community members and leaders are gathered.</p>



<p>It&#8217;s been the year of Gutenberg in 2019, and in this State of the Word, Matt highlights what has been accomplished, and what comes next.</p>



<p>Matt kicks off the event with the premiere of <a href="http://open.film/"><em>Open: The Community Code</em></a>, a film about the open source WordPress community.</p>



<p>Open was really well done, and makes a great intro for this year in review. In the film, Matt said that $10 billion flow through the WordPress economy per year. Yet, it&#8217;s just one of the things that make the WordPress community. WordPress is more than code, more than business. Matt said, &#8220;it&#8217;s more than a tool,&#8221; and that it&#8217;s more like a canvas that, &#8220;anyone can paint on.&#8221; </p>



<p>Open is a film about WordPress, and what WordPress really is &#8212; not only software, but people, a community, and a mission. Open is, &#8220;is a documentary short created by Producer Andrew Rodriguez and Director Sean Korbitz. ​Executive producers Mark Maunder and Kathy Zant contributed to the production of Open.&#8221;</p>



<p>They are submitting the documentary to various festivals. Big shout-out to the <a href="https://www.wordfence.com/">WordFence</a> team, the primary producers of this film.</p>



<h3>The WordPress Community</h3>



<p>The community continues to evolve, thrive, and adapt to the changing environment.</p>



<h4>WordCamp US</h4>



<p>The next WordCamp US will be hosted during the workweek, something I&#8217;m personally thankful for, as it opens up the weekend for family. Also, it will not overlap Halloween. Many people this year, myself included, got in late due to spending the holiday Trick-or-treating with family.</p>



<p>47 organizers, 122 volunteers, and 90 speakers made WCUS possible. Bluehost, WooCommerce, Jetpack, and Google are this year&#8217;s lead sponsors. Thank you to everyone!</p>



<h4>Meetups, people, and community endeavors</h4>



<p>There have been more than 5,000 WordCamp events and several do_action WordPress community events as well.</p>



<p>One <a href="https://heropress.com/">HeroPress</a> story per month is now being featured on the WordPress blog. </p>



<p>Matt also honored <a href="https://poststatus.com/footnotes/were-sad-to-report-that-alex/">Alex Mills</a>,  a prolific contributor who passed away this year.</p>



<h4>WordCamps</h4>



<p>There will be a total of 141 WordCamps in 2019, including 34 in brand new cities. There are also 17 KidsCamps happening in 2019.</p>



<p>This year there are at least four WordCamp US speakers under 15 &#8212; speakers younger than WordPress itself.</p>



<p>In 2020, the first WordCamp Asia is happening, to go alongside WordCamp US and WordCamp Europe. WordCamp Asia is in February, in Bangkok, Thailand. The next WordCamp Europe is in Porto, Portugal.</p>



<h4>One year ago</h4>



<p>WordPress 5.0 was released just before WCUS in Nashville (a contentious decision on timing in itself). It was what Matt calls a &#8220;controversial year,&#8221; where we wanted to &#8220;disrupt ourselves.&#8221; Gutenberg was included in core despite a lot of criticism in and outside of the community. He says we learned a lot from the process and it was &#8220;really good practice for future changes we want to make.&#8221;</p>



<p>He says, &#8220;I understand why we had a lot of this feedback,&#8221; and he&#8217;s excited for where we are going.</p>



<h4>Ways to get involved in WordPress</h4>



<p>Matt highlights several ways to get involved in the WordPress community today:</p>



<ul><li>WordCamp contributor days (including the WCUS one tomorrow)</li><li>Installing the <a href="https://wordpress.org/plugins/gutenberg/">Gutenberg</a> plugin, which is now a testing ground for features. About 275,000 people are taking part in this effort.</li><li><a href="https://wordpress.org/plugins/design-experiments/">Design Experiments</a> is a feature plugin to test user interface experiments.</li><li>Matt makes a call for block creation to, &#8220;expand the window for how people are creating WordPress sites today.&#8221; If it&#8217;s JavaScript only it will be able to go easily into the new block directory.</li><li>Help teach others in the community.</li></ul>



<p>When Matt says <em>why</em> we do all this, he says it&#8217;s to &#8220;help open the web.&#8221; He says the open web is like a pendulum that can swing to be more closed or more open over time.</p>



<h4>Five for the future</h4>



<p>There&#8217;s now a dedicated landing page for <a href="https://wordpress.org/five-for-the-future/">Five for the Future</a> to highlight people contributing through this method.</p>



<h3>Core WordPress Development</h3>



<p>There have been two core releases in 2019. WordPress 5.1, &#8220;<a href="https://wordpress.org/support/wordpress-version/version-5-1/">Betty</a>&#8220;, brought the &#8220;Site Health&#8221; screen, amongst other feature adjustments and bug fixes. It also included the Cron API, Site Meta for Multisite, and more.</p>



<p>WordPress 5.2, &#8220;<a href="https://wordpress.org/support/wordpress-version/version-5-2/">Jaco</a>&#8220;, included live block previews, better block management, and the end of the WSOD (White Screen of Death).</p>



<p>WordPress 5.3 will be released November 12th, and includes more than 150 block editor improvements. Also in 5.3 is the beautiful Twenty Twenty theme. Every six months or so, WordPress will now send admin email verifications to help ensure folks are staying up to date.</p>



<p>The minimum version of WordPress is now 5.6.20 (ending support for 5.2 and 5.3) and compatibility is now up to PHP 7.4. More than 80% are now on PHP 7 or higher. Matt highlights the still-urgent need to improve update mechanisms and strategies with webhosts and site owners.</p>



<p>We are currently at 1,122 total unique contributors to WordPress this year. WordPress 5.3 will have more than a hundred contributors than any release before.</p>



<h3>Gutenberg</h3>



<p>There have been more than 20 major Gutenberg releases, and the number of Gutenberg contributors is up to 480 from 200 a year ago. Matt says the number of sites using Gutenberg is up more than 2.7 times, and the we just surpassed 50 million posts created in Gutenberg &#8212; increasing at a rate of about 270,000 posts per day, as tracked by Jetpack and therefore a conservative estimate.</p>



<p>In last year&#8217;s State of the Word Q&amp;A someone asked what percentage of Gutenberg was done, and Matt answered 10%. He says today that he thinks it&#8217;s about 20% done, and highlights how it&#8217;s an ongoing process that he expects to take a decade to build everything he envisions.</p>



<h4>Gutenberg feature improvements.</h4>



<p>There are a lot of improvements happening:</p>



<ul><li>Gutenberg is now fully mobile compatible and many of the core blocks are fully integrated. Offline support and dark mode are also nearly complete.</li><li>The average seconds to load has been cut in have and time to type has reduced from 170ms to 53ms.</li><li>There is now a smoother scrolling motion in Gutenberg.</li><li>Block previews allow you to see what the block you may insert looks like and also allows you to see more information about what the block does.</li></ul>



<p>And other Gutenberg features coming:</p>



<ul><li>Social icons will be able to be put anywhere.</li><li>The navigation menu is now going to be an inline Gutenberg block &#8212; a precursor I think to &#8220;customize all the things&#8221;.</li><li>Gradients are part of the Gutenberg experience.</li><li>Core Gutenberg is getting a button block.</li></ul>



<p>Matt recognizes that we&#8217;ll have thousands of blocks created for Gutenberg, and there will be a block directory which will load inside the Gutenberg editing experience. Patterns will be collections of blocks that people can install in bulk.</p>



<p>Matt says he thinks in the end, people will be able to create just about any  website layout in just a few clicks.</p>



<p>Several use cases of Gutenberg were highlighted, from newsletter editors to WordCamps, newsrooms, Microsoft Word converters, and more.</p>



<h4>Four phases of Gutenberg</h4>



<p>Matt highlights the four phases of Gutenberg and where we are now.</p>



<ul><li><strong>Easier editing</strong>: simply making WordPress editing better, has been the focus of the last year.</li><li><strong>Customization</strong>: we are &#8220;in the thick&#8221; of this process, some of which is complete and some (like full inline site editing) yet to come.</li><li><strong>Collaboration</strong>: real-time editing collaboration is coming in the third phase, something I&#8217;m so excited about.</li><li><strong>Multilingual</strong>: core WordPress and core Gutenberg will have complete multilingual support.</li></ul>



<h3>In conclusion</h3>



<p>Matt&#8217;s slides for the State of the Word are actually built in Gutenberg.</p>



<p>The code for the presentation <a href="https://github.com/wordpress/slides">is on Github</a> for anyone to use.</p>



<p>It&#8217;s been an interesting and challenging year for WordPress. With that, Matt opens up Q&amp;A.</p>



<p><em>Photo by <a href="https://twitter.com/rzen">Brian Richards</a> for Post Status. Also thank you to <a href="https://twitter.com/dimensionmedia">David Bisset</a> for live tweeting the event while I wrote this.</em></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 02 Nov 2019 22:10:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Brian Krogsgard";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:38:"WordPress.org blog: 2019 Annual Survey";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7460";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://wordpress.org/news/2019/11/2019-annual-survey/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1498:"<div class="wp-block-image"><img src="https://i0.wp.com/wordpress.org/news/files/2019/11/image-12-1.png?fit=632%2C281&ssl=1" alt="" class="wp-image-7472" width="329" height="146" /></div>



<p>It’s time for our annual user and developer survey! If you’re a WordPress user or professional, we want your feedback.</p>



<p>It only takes a few minutes to <a href="https://wordpressdotorg.survey.fm/wordpress-2019-survey-english">fill out the survey</a>, which will provide an overview of how people use WordPress. We&#8217;re excited to announce that this year, for the first time, the survey is also available in 5 additional languages:  <a href="https://wordpressdotorg.survey.fm/wordpress-2019-survey-french">French</a>, <a href="https://wordpressdotorg.survey.fm/wordpress-2019-survey-german">German</a>, <a href="https://wordpressdotorg.survey.fm/wordpress-2019-survey-japanese">Japanese</a>, <a href="https://wordpressdotorg.survey.fm/wordpress-2019-survey-russian">Russian</a>, and <a href="https://wordpressdotorg.survey.fm/wordpress-2019-survey-spanish">Spanish</a>. Many thanks to the community volunteers who helped with the translation effort!</p>



<p>The survey will be open for 4 weeks, and results will be published on this blog. All data will be anonymized: no email addresses or IP addresses will be associated with published results. To learn more about WordPress.org’s privacy practices, check out the <a href="https://wordpress.org/about/privacy/">privacy policy</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 02 Nov 2019 21:15:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Andrea Middleton";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:76:"WPTavern: BoldGrid Joins Forces with W3 Edge, Acquires W3 Total Cache Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=95188";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:86:"https://wptavern.com/boldgrid-joins-forces-with-w3-edge-acquires-w3-total-cache-plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5167:"<p><a href="https://www.boldgrid.com">BoldGrid</a>, a WordPress product and services company, announced today that it has joined forces with <a href="https://www.w3-edge.com/">W3 Edge</a>, the parent company behind the popular <a href="https://wordpress.org/plugins/w3-total-cache/">W3 Total Cache</a> (W3TC) plugin.  BoldGrid acquired the W3TC plugin and retained its creator, Frederick Townes, along with its development and support staff.  The two teams will operate as sister companies but jointly work on the plugin&#8217;s future roadmap.</p>



<p>&#8220;There are some things a larger team can accomplish for Total Cache that we are targeting right now,&#8221; said Brad Markle, development manager at BoldGrid.  &#8220;After a few more releases of core Total Cache features, the TC team is slated to help accelerate our CRIO Theme Framework on the BoldGrid side.&#8221;</p>



<p>BoldGrid has a range of plugins, themes, and services for WordPress sites.  The company offers plugins like its post and page builder, themes such as its &#8220;super theme&#8221; CRIO, and services like testing and performance coaching.</p>



<p>W3 Edge&#8217;s primary product is its W3TC plugin, which serves as the caching solution for over one million WordPress installs.  It is one of the most-used caching plugins available in the official WordPress plugin directory.  </p>



<p>Many competing caching plugins have been gaining considerable exposure in the past few years.  Some of those are free.  Others, such as WP Rocket, have captured large segments of the premium market.  Managed WordPress hosts also generally offer built-in caching solutions as part of their strategy to build their customer numbers.  The question is whether this move will provide growth for W3TC and any related products or services.</p>



<h2>The Future of the W3TC Plugin</h2>



<div class="wp-block-image"><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/10/w3tc-stats.png?ssl=1" target="_blank" rel="noreferrer noopener"><img /></a>Screenshot of a new Caching Statistics page for W3 Total Cache</div>



<p>The BoldGrid team has plans to continue developing the W3TC plugin.  &#8220;Since joining with the awesome team at W3, we have been working to add in some slick new features like Caching Statistics and Lazy Loading,&#8221; said Harry Jackson, product manager at BoldGrid.</p>



<p>&#8220;We are also looking to partner with theme and plugin developers to ensure the widest range of compatibility for the product, and the WordPress Community,&#8221; said Jackson.  It is unclear what such partnerships would entail and the type of compatibility needed from third-party developers.  The BoldGrid team did not provide further details.</p>



<p>For some users, the W3TC interface and options can be overwhelming.  &#8220;User Experience is at the top of the list of things we are working on,&#8221; said Sash Ghosh, BoldGrid&#8217;s marketing manager.  &#8220;It can be challenging for some users to fully understand and utilize all the powerful features.  We will soon be adding an on-boarding and configuration guide to the plugin that will hopefully make the plugin accessible to more users.&#8221;</p>



<h2>Building Trust After a Rocky Past</h2>



<p>Despite setbacks in 2016, the W3TC plugin has maintained over one million active installs over the past three years.  In March of that year, there was <a href="https://wptavern.com/frederick-townes-confirms-w3-total-cache-is-not-abandoned">concern that the plugin was abandoned</a> after no activity for seven months.  The plugin was not working for many users on the latest version of WordPress.</p>



<p>Much of the issue seemed to stem from not yet knowing how to scale such a popular product with a small team.</p>



<p>Later in September of 2016, a <a href="https://wptavern.com/high-risk-xss-vulnerability-discovered-in-w3-total-cache-plugin">high-risk XSS vulnerability</a> was discovered with the plugin.  The plugin developer <a href="https://wptavern.com/w3-total-cache-0-9-5-packages-xss-vulnerability-patch-with-major-update">patched the plugin</a> quickly.  However, the updated versions introduced new bugs and a poor experience for many users.</p>



<p>While things seem to have been running more smoothly in recent years, there is still some level of distrust within the inner WordPress community.  When asked how they are prepared to address past issues and assure they are looking out for the best interests of users in the future, the BoldGrid team said that security is a top priority.  They also expressed their openness to community feedback for improvement.</p>



<p>&#8220;As with all big plugins, there are challenges with functionality, features, and security,&#8221; said Jackson.  &#8220;With a bigger team and additional Quality Assurance resources we feel that Total Cache will continue to improve in all the major areas.  We have also introduced a public pull request process to facilitate additional feedback and bug fixes.  Though you can&#8217;t ever guarantee security, our team is very committed and respects our responsibilities to our million-plus users.&#8221;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 01 Nov 2019 14:01:49 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:71:"Akismet: Version 4.1.3 of the Akismet WordPress Plugin is Now Available";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"http://blog.akismet.com/?p=2058";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://blog.akismet.com/2019/10/31/akismet-plugin-4-1-3/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:640:"<p>Version 4.1.3 of <a href="http://wordpress.org/plugins/akismet/">the Akismet plugin for WordPress</a> is now available. It contains the following changes:</p>



<ul>
<li>We&#8217;ve improved the activation and setup process.</li>
<li>We&#8217;ve fixed a bug that could have allowed an attacker to make you recheck your Pending comments for spam.</li>
</ul>



<p>To upgrade, visit the Updates page of your WordPress dashboard and follow the instructions. If you need to download the plugin zip file directly, links to all versions are available in <a href="http://wordpress.org/plugins/akismet/">the WordPress plugins directory</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 31 Oct 2019 15:59:19 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"Christopher Finke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:24:"Matt: New Automattic CFO";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"https://ma.tt/?p=50302";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:41:"https://ma.tt/2019/10/new-automattic-cfo/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:975:"<p>As <a href="https://venturebeat.com/2019/10/16/automattic-nabs-vivint-cfo-to-chase-steep-revenue-growth/">Venturebeat has picked up</a>, Mark Davies will be leaving <a href="https://www.vivint.com/">Vivint</a> and joining <a href="https://automattic.com/">the merry band</a>. <span>Automattic is creating the operating system for the web, from <a href="https://wordpress.com/">websites</a> to <a href="https://woocommerce.com/">ecommerce</a> to <a href="https://tumblr.com/">social networks</a>. As we zoom past 1,100 employees in over 70 countries, we wanted a financial leader with experience taking businesses from hundreds of millions in revenue to billions (Vivint) and even tens of billions (Alcoa and Dell), <a href="https://www.linkedin.com/in/mark-davies-14937a3/">as Mark has</a>. I’m excited about working alongside such an experienced leader day-to-day to build what I hope will become one of the defining technology companies of the open web era.</span></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Oct 2019 15:28:13 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:29:"HeroPress: 14 Years’ Detour";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:56:"https://heropress.com/?post_type=heropress-essays&p=2972";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:104:"https://heropress.com/essays/14-years-detour/#utm_source=rss&utm_medium=rss&utm_campaign=14-years-detour";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:35176:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2019/10/100219-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull Quote: I learned not only to keep face and move forward, but also to dare." /><p><a href="https://heropress.com/feed/#ru">Это эссе также доступно на русском языке</a></p>
<blockquote><p>Knowing what you want isn&#8217;t enough.</p></blockquote>
<p>At the age of fifteen, I was told to hurry up with my professional education, if I don’t get a free place at university no one will pay for me. Last one was obvious and not because of money. I didn’t know how high or low my chances are even I had very good marks. I could have been just the biggest fish in the pond, but I wanted to leave people who treat me like expense item as soon as possible anyway. I made up my mind and it’s separated me from my classmates. I got secondary school certificate and left school instantly without goodbyes.</p>
<p>My knowledge of opportunities was very narrow. I thought to be an ecologist or guide translator from German, based on what I was taught at school, or an advertiser, it was ads boom in Russia, ads were fascinating, and I collected flyers. At the same time, I was already good a computer user and did a lot of typing for one of the school&#8217;s principals, sometimes instead of classes, volunteered once and they couldn’t say “no”.</p>
<p>No one bothered to give me advice, but I was sure my future was unimaginable without computers and came to a general decision to be a programmer. The range of technical schools was not wide, and names of specializations looked very abstruse. I messed up and spend four years studying transistor markings, soldering and drawing PCB layouts. Programming course turns out to be a bit of a joke, it was Pascal, we didn’t even try to do something useful with it. English course was another flaw in this education, the teacher was busy showing off and teasing girls. I&#8217;m not sure if he even knew the subject. He was fired after our collective petition. We got good marks just to forget about it. I left technical school with honors degree and improved typing skills.</p>
<p>It was wild unfriendly market I faced. I didn’t know how to recognize a normal job offer from sinister one, so I bumped into several, like banging in people’s doors and pitch them in buying cheap stuff for very high price. It was harassing and I have no idea how long I would’ve looked for work without help.</p>
<h3>The wrong approach can cause failure</h3>
<p>I was lucky to have a father in an IT company. He proposed me like a substitute to typists when I was still a student and when I finished, helped me to get a job on updating a law system on client’s computers. Maybe I could get it by coming from the street, but I had to know where the entrance is. Half a year later I got a full-time job in the same service department and started to play volleyball with colleagues. It was great to meet people from other departments and on the field was no big difference between director and analyst, it’s fair play. I liked my place and my clients, but I was “friendly” told that without a university degree I will have no further promotions.</p>
<p>At this time, I tried to study PHP by a book. It was very exciting at first, but a lot of functions without explanation how to build something useful with them didn’t make much sense and when I tried, I failed and backed off. It was hard to admit a fail even to myself and it was nagging me for a long time.</p>
<p>I had to choose something I can handle; I’m interested in and can afford. It turned out to be advertising. I spent most of my and my husband’s holidays on sessions in the next 6 years. It was tricky for him to make me to leave a computer, once I was glued to it, so he bought me my first laptop. English was still hard for me, I got my high marks just memorizing all the words in a textbook and how they must sound. Again, I wanted something I cannot handle.</p>
<blockquote><p>I started to hate my workplace long before I finished education with another honor degree no one was interested in.</p></blockquote>
<p>I got a promotion quite soon despite apprehensions but then my chief was dismissed and with new one the things became very tense; I escaped to have a baby and spent a whole three years on child’s care holidays full of doubts. I tried to get off by studying, drawing and baking but the pram was pulling me back. I didn’t use to stay put, rely on my husband’s money and be separate from other people. I didn’t want to come back, but it looked like I have no better choice, I was convinced (overall, rightly) that not too many companies want in office position a woman with small baby and lots of sick leaves.</p>
<p>After I returned at work it became clear that the situation in my department was unhealthier than ever, I lost my place and next boss treated all back-office girls like pieces of furniture. In a few months I had enough.</p>
<h3>The flip side of the coin can become a black swan</h3>
<p>I wanted to be a marketer. Knowing how tricky it is to sell intangibles, I wanted solid product to work with. Now I see that it isn&#8217;t a point at all.</p>
<p>It turned out to be difficult to find a job outside IT, some HRs was kind enough to answer that with my experience I’ll better be in IT.</p>
<p>Still I was very hopeful, studied hard and considered myself well, but once again I set a low bar to my employers. Companies I worked in wanted to get all publicity and sales rise through a cost of my salaries. I was careless once, the next time I asked specifically about budget before signing up and was assured, but still they meant my wages. It was a tough period of disappointments.</p>
<p>When I was offered a part-time administrative job with “ok” to sick leaves, I took it gladly like a reprieve. It was far from home, and I was spending 2-3 hours a day on buses with Harry Potter audio books for company. In these traffic jams I started to feel English at last and loved it, it gave me a freedom no money can buy. And despite the long way I managed to play volleyball with my husband and his colleagues. Life was getting better.</p>
<p>This job itself in addition to low payment had something valuable to me — a working website. After my boss had a row with its developer, I got it to maintain, did some reverse engineering and understood how it works. It wasn’t a most creative site, but it gave me a view. I started to write simple sites from scratch.</p>
<blockquote><p>My first JavaScript calculator almost made me crazy, but I pursued.</p></blockquote>
<p>From time to time I was asked for help from a friend or relative, usually to solve some urgent problems. So, I started to meet popular CMSs. One of the first I met with was WordPress. There was some issue in theme, which was changed and dropped by developer. I was digging a whole weekend deciphering how it works and found infinity loop to fix. Back then for me it was just a system…</p>
<p>Two years later I found myself still clinging to my temporary job. I was tired of working for a hard nosed dictator, the last drop was his statement that I was not a programmer, because he hadn’t seen anything I made. I’ve already written some parts of website he asked for, so it was just unfair. I became angry and it was exactly what I needed, a big kick.</p>
<p>I went out but still had no courage to pretend on a developer’s place and landed in some franchise company selling “box version” websites. It was another tough half of a year with a lot of work, low payment and plan failures, ending with pneumonia. I see now that I was making a disservice to customers, websites are not a microwave meal — quick, cheap and dummy. There was no life in them without a lot of work no one bargained to buy or do. Most of the sites I sold back then died when year expired but they never were truly alive and useful.</p>
<h3>You need to pluck up your courage to become lucky</h3>
<p>When I recovered, I search through developer’s job offers, but it was difficult to find something suitable worth trying to apply. I was reading job advertising and it looked, and still is, like mostly IT company are presented and they want geniuses who know a lot of technologies and frameworks at once. It was very distressing just to look.</p>
<p>And then I became lucky again. I opened a private ads site and almost immediately found a job which was fitted me perfectly — they wanted someone with experience to write from scratch, understand another’s code and maintain it, ability to translate technical documentation and articles and make simple design of printing products. I made test task and there was no need in my resume or diplomas, I was taken. It’s turned out direct ad from one of sales departments in tech company and I passed by HR, who most likely wouldn&#8217;t even have considered me. Superior agreed to have me remotely most of the time, it solved sick leaves problem even if it was already much better than before. Addition to better salaries and calm work without over hours, I got very pleasant colleagues. We are friends ever since, despite back then no one guessed to invite me to play volleyball.</p>
<p>It was 14 years after the original decision to be a programmer and it was only the beginning. I left this amazing place a couple years later when it held no more challenges for me.</p>
<h3>Conscious decisions require wide knowledge</h3>
<p>After I worked with a bunch of CMSs, I started to be able to compare them and understood not only that WordPress is the best one for developers and clients but also that I didn’t see right examples at all. The biggest flaw of WordPress — it’s so easy to make things work that there&#8217;s no need to bother and do things right and this becomes a problem later. I also saw bad cases on very different systems… and did them as well.</p>
<p>I used to work relying on examples at hand, documentation and Google, but searching for a specific feature or a solution, I found myself again missing the whole picture. At this point Udemy courses came very handy and then I started to attend WordPress events, firstly online and then by foot, trains and airplanes and discovered a wide and very alive community. Now I know not only where to look but whom to ask and how to be not far from those who stays on top of things as much as possible. Most important is that I found allies who don’t think I’m going crazy, speaking with shining eyes about work, with whom I share a passion and fondness to WordPress. It’s what matters.</p>
<p>Now, after 6 years of full time in development, I still feel myself like a newbie, it’s endless learning, frequent discoveries, mistakes and impassable wish to do better…</p>
<h3>The way is the destination</h3>
<p>I made a path very uneven, a lot of mess and banging, but for me it’s like a kaleidoscope where a little turn presents a new picture, new “a-ha” moment, new excitement after seemingly pointless efforts. When in doubt I remind myself about David Ogilvy who tried a lot of things before struck gold with advertising and it’s maybe why.</p>
<p>Finally, I learned not only to keep face and move forward but also to dare.</p>
<h3>Freedom is to make your own mind</h3>
<p>I left my last workplace after we finished exhausted two years project on another CMS system which was a big reinventing of wheels and made up my mind to work only with WordPress from now on and dare to be my own boss.</p>
<p>Now I’m officially an entrepreneur. This big boy’s stuff looks difficult. But I don’t want to be told, collaboration is a new black wherever I look.</p>
<h2 id="ru">Крюк длинной в 14 лет</h2>
<blockquote><p>Не достаточно знать, что хочешь</p></blockquote>
<p>В 15 лет мне было сказано, чтобы я поспешила с профессиональным образованием, если не поступлю бесплатно в институт, никто платить за меня не будет. Последнее было очевидно и не из-за денег. Несмотря на весьма хорошие оценки, я не могла оценить свои шансы, могло оказаться, что я просто большая рыба в маленьком пруду. Но в любом случае, я хотела как можно быстрей расстаться с людьми, видящими во мне одну большую статью расходов. Я приняла решение и это откололо меня от моих одноклассников. Ушла из школы я, не прощаясь, как только получила на руки сертификат об окончании 9 классов.</p>
<p>Нужно было выбрать профессию, но потенциальные возможности были весьма туманны. Я могла стать экологом или немецкоязычным гид-переводчиком, на основе того, чему училась в школе, или рекламщиком, в России тогда начался рекламный бум, и она казалась очень захватывающей, я даже листовки собирала. В то же самое время я уже была хорошим пользователем компьютера и набирала документы для завуча старших классов, иногда вместо занятий — вызвалась один раз и потом уже не могла сказать «нет».</p>
<p>Никто не пытался дать мне совет, но я была уверена, что будущее немыслимо без компьютеров и решила стать программистом. Выбор техникумов в пределах разумной досягаемости, был не очень большой, а названия специальностей выглядели очень мудрено. Я ошиблась и провела следующие 4 года изучая маркировку транзисторов, паяя и чертя печатные платы. Курс программирования обернулся дурной шуткой, это был Паскаль, и мы даже не пытались сделать с ним что-то полезное. Другим серьезным недостатком этого обучения стал английский язык, преподаватель выпендривался и задирал девушек, я так и не поняла знал ли он язык вообще. В конце концов он был уволен после нашей коллективной жалобы, а мы получили хорошие оценки, в качестве решения проблемы. Я выпустилась из техникума с красным дипломом и улучшенными навыками машинистки.</p>
<p>Рынок труда был диким и неизведанным. Не зная, как распознать сомнительное предложение о работе, наткнулась на несколько таких, как ходить по домам и навязывать людям дешевые утюги за внушительную цену. Поиск работы оказался изматывающим, без посторонней помощи, я могла бы еще долго ходить по этим собеседованиям.</p>
<h3>Неправильный подход может быть причиной неудачи</h3>
<p>Мне повезло, отец работал в IT-компании. Он предложил меня в качестве подмены наборщикам, когда я еще училась, а когда закончила, помог получить работу по обновлению правовой системы на компьютерах у клиентов. Может быть меня взяли бы и, приди я с улицы, но для этого нужно было знать, куда идти. Через полгода я получила место в офисе в том же отделе обслуживания, а также начала играть с коллегами в волейбол. Это было здорово, знакомиться с людьми из других отделов, а еще на поле нет большой разницы между директором и аналитиком. Мне нравилось мое место и мои клиенты, но опять мне было «дружески» сказано, что без высшего образования, на большее я могу не рассчитывать.</p>
<p>В это время я пыталась изучать PHP по книге. В начале все выглядело очень захватывающе, но функции без понимания как из них построить что-то целое не имели большого смысла, я пробовала, у меня не получалось, и в конце концов сдалась. Было сложно признаться, что я не смогла, даже самой себе и это неприятное чувство преследовало меня долгое время.</p>
<p>Нужно было выбрать то, с чем я смогу справиться, что-то интересное и что будет мне по карману. Выбор пал на рекламу. В следующие 6 лет большая часть наших с мужем отпусков ушла на мои сессии. Ему было сложно выгнать меня из-за компьютера, я к нему приклеилась, поэтому он купил мне мой первый ноутбук. Английский по-прежнему давался мне очень трудно, и чтобы получить пять, пришлось переводить и запоминать вместе с транскрипцией все слова в учебном пособии. Снова я хотела то, что мне не давалось.</p>
<blockquote><p>Я начала ненавидеть свое рабочее место задолго до того, как закончила университет с еще одним никому не нужным красным дипломом.</p></blockquote>
<p>Я получила повышение достаточно скоро, несмотря на опасения, но затем руководство сменилось и работать с новым стало весьма напряженно. Я сбежала в декрет и провела следующие три года отпуска по уходу за ребенком полные сомнений. Учеба, рисование и приготовление пирогов отвлекали, но я оказалась не готова быть привязанной к коляске — ограниченной в передвижениях, оторванной от других людей, полагаться на деньги мужа. Возвращаться на работу я не хотела, но не видела лучшего выхода, была убеждена, и вполне резонно, что ни так много компаний готовы взять на офисную позицию женщину с маленьким ребенком и кучей больничных.</p>
<p>После того, как я вернулась на работу, стало ясно, что ситуация стала еще хуже, чем была. Я потеряла свое место, а очередной новый руководитель относился во всем девушкам «поддержки» как к мебели. Несколько месяцев мне хватило.</p>
<h3>Обратная сторона медали может быть золотой</h3>
<p>Я хотела быть маркетологом. Зная, как непросто продавать что-то неосязаемое, решила работать с товаром, который можно пощупать. Сейчас думаю, разница была только в голове.</p>
<p>Оказалось, сложно найти работу вне IT-сектора, некоторые менеджеры по персоналу снисходили для объяснений, что мне будет лучше в IT.</p>
<p>И все-таки я была настроена оптимистично, я усердно училась и считала, что хорошо справляюсь, но опять, оказалось, что установила слишком низкую планку для работодателей. Компании, в которых я работала, хотели и публикации, и рост продаж исключительно за счет моей зарплаты. Ошибившись один раз, в следующий я специально уточнила вопрос с бюджетом, и меня заверили, что он есть. Оказалось, что это по-прежнему только зарплата. Это был период разочарований.</p>
<p>Когда мне предложили административную работу на полставки с терпимым отношением к больничным, я с радостью ухватилась за возможность взять передышку. Работа была далеко от дома, и я проводила 2-3 часа в маршрутках каждый день в компании с аудиокнигами про Гарри Поттера. В этих дорожных пробках я наконец почувствовала английский язык и полюбила его, это дало мне свободу, которую невозможно купить за деньги. И, несмотря на долгую дорогу, я смогла играть после работы в волейбол с мужем и его коллегами. Жизнь налаживалась.</p>
<p>Эта работа сама по себе, помимо низкой зарплаты, имела нечто ценное для меня – работающий сайт. После того, как мой начальник поссорился с разработчиком, я получила его на поддержку. Разобрав его на части, я поняла, как он работает. Это не был самый креативный сайт в мире, но он дал мне целостное представление, и я начала писать простые сайты с нуля.</p>
<blockquote><p>Мой первый калькулятор на JavaScript практически свел меня с ума, но я продолжила.</p></blockquote>
<p>Время от времени меня просили помочь родственники и друзья, обычно решить какую-то срочную проблему. Так я стала встречаться с популярными CMS. Одной из первых оказался WordPress. Там была проблема в теме, которую разработчик изменил и бросил. Я копала все выходные, но в конце концов нашла место, где код уходил в бесконечный цикл и исправила. Тогда для меня это была просто какая-то система…</p>
<p>Два года спустя я все еще держалась за свою временную работу. Резкие манеры начальника перестали казаться забавными и последней каплей стало его категорическое заявление, что я не программист, потому что он не видел ничего, созданного мной. На тот момент я уже написала для сайта функционал, который он же просил, так что это было обидно. Я разозлилась и это стало толчком в нужном направлении.</p>
<p>Я ушла, но все еще не пыталась получить работу разработчика, и в результате оказалась в одной из франшизных компаний, продающих сайты «из коробки». Это были сложные полгода с кучей работы, низкой зарплатой и провалами плана, закончившиеся воспалением легких. Сейчас, думаю, я оказывала клиентам медвежью услугу, сайт – не готовое блюдо для микроволновки, быстрое, дешевое и типовое. В этих сайтах нет жизни без вложения огромного труда, за которых никто не готов был платить. Большая часть сайтов умерли через год, но они и живыми то не были.</p>
<h3>Нужно набраться смелости, чтобы повезло</h3>
<p>Когда я поправилась, начала искать работу разработчика, но было сложно найти что-то подходящее даже просто чтобы решиться ответить на вакансию. В объявлениях были в основном представлены IT-компании, которым нужны гении, знающие огромное количество технологий и фреймворков. Только вид этих вакансий вгонял в депрессию.</p>
<p>А потом мне снова повезло. Я открыла сайт частных объявлений и буквально сразу же нашла работу, которая мне подходила идеально — они хотели кого-то с опытом написания с нуля, умеющего разбираться и дописывать чужой код, переводить техническую документацию и статьи, а также делать простую полиграфию. Я сделала тестовое задание и была принята, ни дипломы, ни резюме уже не понадобились. Оказалось, это был объявление одного из отделов продаж в компании, занимающейся промышленным оборудованием, и я прошла в обход отделка кадров, который, уверена, даже не посмотрел бы на меня. Начальник согласился на мою работу удаленно, что решило проблему частых больничных, хотя ребенок рос, и их уже стало намного меньше. В дополнение к лучшей зарплате и спокойной работе без переработок и нервотрепки, мне достались замечательные коллеги. Мы дружим с тех пор, несмотря на то, что тогда никто не догадался позвать меня играть в волейбол.</p>
<p>Это случилось через 14 лет после первоначального решения стать программистом и это был только начало. Я ушла через пару лет, когда ничего нового в работе уже не осталось.</p>
<h3>Сознательные решения требуют широкий знаний</h3>
<p>После того, как я поработала с разными CMS, смогла не только сравнить их между собой и понять, что WordPress лучшая из всех как для разработчиков, так и для клиентов, но также обнаружить, что вообще не видела хороших примеров разработки. Самая большая проблема WordPress — сделать так, чтобы работало, настолько легко, что мало кто не утруждает себя делать правильно, что позже оборачивается проблемами при доработке и поддержке. Впрочем, я видела плохие решения на разных CMS, да и сама их делала.</p>
<p>Отталкиваясь от доступных примеров и документации и ища в поисковиках конкретные решения, я опять обнаружила, что не вижу всей картины. В этот момент курсы Udemy оказались очень кстати, а затем я начала посещать мероприятия WordPress, сначала онлайн, потом топая ни них ножками, оправляясь на поездах и самолетах, и обнаружила большое и весьма активное сообщество. Наконец-то я нашла не только тех, у кого можно спросить, но и как быть недалеко от тех, кто «в теме», насколько это вообще возможно. Самое важное – я нашла единомышленников, тех, кто не думает, что человек, говорящий с горящими глазами о работе, рехнулся, с кем у нас общая страсть и любовь к WordPress. Это то, что имеет значение.</p>
<p>Сейчас, проработав 6 лет как разработчик, я все еще чувствую себя новичком, это бесконечное обучение, частые открытия, ошибки и непроходящее желание сделать лучше.</p>
<h3>Смысл пути в самом пути</h3>
<p>Мой путь очень извилистый, много метаний и набитых шишек, но для меня это как калейдоскоп, где каждый поворот показывает новую картинку, новое озарение, дает энергию двигаться дальше после казавшихся напрасными усилий. В периоды сомнений, я напоминаю себе о Дэвиде Огилви, который сменил множество специальностей, пока не добился успеха в рекламе.</p>
<p>Я научилась не только сохранять лицо и идти вперед, но также набралась смелости.</p>
<h3>Свобода — возможность принимать решения</h3>
<p>Я ушла со своего последнего места работы, где мы закончили изматывающий двухлетний проект на другой CMS, ставший сам по себе большой ошибкой, и ни только приняла решение работать исключительно с WordPress, но и стать своим собственным начальником.</p>
<p>Теперь я официально предприниматель. Все эти вещи «для больших мальчиков» выглядят достаточно сложными, но я больше не хочу слепо делать то, что мне скажут. В тренде коллаборации, куда ни глянь.</p>
<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: 14 Years’ Detour" class="rtsocial-twitter-button" href="https://twitter.com/share?text=14%20Years%E2%80%99%20Detour&via=heropress&url=https%3A%2F%2Fheropress.com%2Fessays%2F14-years-detour%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: 14 Years’ Detour" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fessays%2F14-years-detour%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fessays%2F14-years-detour%2F&title=14+Years%E2%80%99+Detour" rel="nofollow" target="_blank" title="Share: 14 Years’ Detour"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/essays/14-years-detour/&media=https://heropress.com/wp-content/uploads/2019/10/100219-150x150.jpg&description=14 Years’ Detour" rel="nofollow" target="_blank" title="Pin: 14 Years’ Detour"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/essays/14-years-detour/" title="14 Years’ Detour"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/essays/14-years-detour/">14 Years’ Detour</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 02 Oct 2019 05:00:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Olga Gleckler";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"BuddyPress: BuddyPress 5.0.0 “Le Gusto”";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=308041";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://buddypress.org/2019/09/buddypress-5-0-0-le-gusto/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:10760:"<p>Here&#8217;s our latest major release featuring the <strong>BuddyPress REST API</strong> !!</p>



<div class="wp-block-button aligncenter is-style-squared"><a class="wp-block-button__link has-background" href="https://downloads.wordpress.org/plugin/buddypress.5.0.0.zip">Get BuddyPress 5.0.0</a></div>



<div class="wp-block-spacer"></div>



<p>We are very excited to announce the BuddyPress community the immediate availability of <strong>BuddyPress 5.0.0</strong> code-named &#8220;<strong>Le Gusto</strong>&#8220;. You can get it clicking on the above button, downloading it from our&nbsp;<a href="https://wordpress.org/plugins/buddypress/">WordPress.org plugin repository</a> or checking it out from our <a href="https://buddypress.trac.wordpress.org/browser/branches/5.0">subversion repository</a>.</p>



<p><em>NB: if you&#8217;re upgrading from a previous version of BuddyPress, please make sure to back-up your WordPress database and files before proceeding.  </em></p>



<p>You can view all the changes we made in 5.0.0 thanks to our <a href="https://codex.buddypress.org/releases/version-5-0-0/">full release note</a>. Below are the key features we want to get your attention on.</p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-rest-api"></span></div>



<div class="wp-block-spacer"></div>



<h2>The BP REST API opens a new era for BuddyPress!</h2>



<p>You can now enjoy&nbsp;REST&nbsp;API&nbsp;endpoints&nbsp;for&nbsp;members, groups, activities, private&nbsp;messages, screen notifications and extended profiles.</p>



<p>BuddyPress endpoints provide machine-readable external access to your WordPress site with a clear, standards-driven interface, paving the way for new and innovative methods of interacting with your community through plugins, themes, apps, and beyond.</p>



<p>The BP REST API opens great new opportunities to improve the way you play with the BuddyPress component features: we couldn&#8217;t resist to start building on top of it introducing&#8230; </p>



<h3>A new interface for managing group members.</h3>



<div class="wp-block-image"><a href="https://buddypress.org/wp-content/uploads/1/2019/09/group-manage-members.png"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/group-manage-members.png" alt="Screen Capture of the new Group Mange Members UI" class="wp-image-308052" /></a></div>



<p>Group administrators will love our new interface for managing group membership. Whether you&#8217;re working as a group admin on the front-end Manage tab, or as the site admin on the Dashboard, the new REST API-based tools are faster, easier to use, and more consistent.</p>



<h3>The BP REST API is fully documented</h3>



<p>The development team worked hard on the features but also took the time to <a href="https://buddypress.org/2019/09/bp-devhub-1-0/">write the documentation</a> about how to use it and how to extend it. BuddyPress developers, let&#8217;s start building great stuff for our end users: take a look at <a href="https://developer.buddypress.org/bp-rest-api/">the BP REST API developer reference</a>.</p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-buddicons-groups"></span></div>



<div class="wp-block-spacer"></div>



<h2>Improved Group invites and membership requests</h2>



<p>Thanks to the new BP Invitations API, Group invites and membership requests are now managed in a more consistent way. The BP Invitations API abstracts how these two actions are handled and allows developers to use them for any object on your site (e.g., Sites of a WordPress network).</p>



<p>Read&nbsp;more&nbsp;about&nbsp;the&nbsp;<a href="https://bpdevel.wordpress.com/2019/09/16/new-invitations-api-coming-in-buddypress-5-0/">BP&nbsp;Invitations&nbsp;API</a>.</p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-buddicons-forums"></span></div>



<div class="wp-block-spacer"></div>



<h2>Help our support volunteers help you.</h2>



<p>Knowing your WordPress and BuddyPress configuration is very important when one of our beloved support volunteers tries to help you fix an issue. That&#8217;s why we added a BuddyPress section to the Site Health Info Administration screen.</p>



<a href="https://buddypress.org/wp-content/uploads/1/2019/09/debug-buddypress.png"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/debug-buddypress.png" alt="Screen capture of the BuddyPress section of the Site Health screen." class="wp-image-308058" /></a>



<p>The panel is displayed at the bottom of the screen. It includes the BuddyPress version, active components, active template pack, and a list of other component-specific settings information.</p>



<div class="wp-block-spacer"></div>



<div class="wp-block-columns has-2-columns">
<div class="wp-block-column">
<div><span class="dashicons dashicons-heart"></span></div>
</div>



<div class="wp-block-column">
<div><span class="dashicons dashicons-wordpress-alt"></span></div>
</div>
</div>



<div class="wp-block-spacer"></div>



<h2>Improved integrations with WordPress</h2>



<h3>BP Nouveau Template Pack</h3>



<p>In BuddyPress 5.0.0, the BP Nouveau template pack looks better than ever with the Twenty Nineteen theme.</p>



<div class="wp-block-image"><a href="https://buddypress.org/wp-content/uploads/1/2019/09/edit-password.png"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/edit-password.png" alt="" class="wp-image-308069" /></a></div>



<p>Nouveau now uses the same password control as the one used in WordPress Core, for better consistency between BuddyPress and WordPress spaces.</p>



<h3>BuddyPress Blocks now have their own category into the Block Editor.</h3>



<div class="wp-block-image"><a href="https://buddypress.org/wp-content/uploads/1/2019/09/bp-blocks.png"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/bp-blocks.png" alt="" class="wp-image-308070" /></a></div>



<p>Developers building tools for the Block Editor can now add their blocks to the BuddyPress category. This change provides a foundation for organizing custom BuddyPress blocks.</p>



<p>Read more about this feature in this <a href="https://bpdevel.wordpress.com/2019/07/31/a-category-to-store-your-buddypress-blocks/">development note</a>.</p>



<div class="wp-block-image"><a href="https://buddypress.org/2018/11/buddypress-4-0-0-pequod/#comment-44752"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/matt-comment.png" alt="" class="wp-image-308075" /></a>Screen capture of the <a href="https://buddypress.org/2018/11/buddypress-4-0-0-pequod/#comment-44752">comment</a> Matt made about BuddyPress 4.0.0</div>



<p><em>PS: we know, just like Matt, you&#8217;re eager to enjoy high quality community blocks: now we have the BP REST API and this new Blocks category available in BuddyPress Core, get ready to be amazed for our next release. Fasten your seatbelts: BuddyPress blocks are arriving!</em></p>



<div class="wp-block-spacer"></div>



<div class="wp-block-image"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/pizza.png" alt="" class="wp-image-308073" /></div>



<h2>BuddyPress Le Gusto</h2>



<p>5.0.0 is code-named <strong>&#8220;Le Gusto&#8221;</strong> after the <a href="https://goo.gl/maps/tpvew6YSivZ5KX218">well known Pizza restaurant</a> in Fortaleza, Brazil. It’s the perfect place to meet with friends and start tasting new flavors like <a class="bp-suggestions-mention" href="https://buddypress.org/members/espellcaste/" rel="nofollow">@espellcaste</a>’s favorite one: the &#8220;Pizza de Camarão&#8221;. </p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-buddicons-buddypress-logo"></span></div>



<div class="wp-block-spacer"></div>



<h2>Muito Obrigado</h2>



<p>As usual, this BuddyPress release is only possible thanks to the contributions of the community. Special thanks to the following folks who contributed code and testing to the release: <a href="https://github.com/baconbro">baconbro</a>, <a href="https://profiles.wordpress.org/boonebgorges/">Boone B Gorges (boonebgorges)</a>, <a href="https://profiles.wordpress.org/joncadams/">boop (joncadams)</a>, <a href="https://profiles.wordpress.org/sbrajesh/">Brajesh Singh (sbrajesh)</a>, <a href="https://profiles.wordpress.org/dcavins/">David Cavins (dcavins)</a>, <a href="https://profiles.wordpress.org/ericlewis/">Eric Lewis (ericlewis)</a>, <a href="https://profiles.wordpress.org/geminorum/">geminorum</a>, <a href="https://profiles.wordpress.org/gingerbooch/">gingerbooch</a>, <a href="https://profiles.wordpress.org/ivinco/">Ivinco</a>, <a href="https://profiles.wordpress.org/whyisjake/">Jake Spurlock (whyisjake)</a>, <a href="https://profiles.wordpress.org/JarretC/">Jarret (JarretC)</a>, <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby (johnjamesjacoby)</a>, <a href="https://profiles.wordpress.org/klawton/">klawton</a>, <a href="https://profiles.wordpress.org/kristianngve/">Kristian Yngve (kristianngve)</a>, <a href="https://profiles.wordpress.org/maniou/">Maniou</a>, <a href="https://profiles.wordpress.org/netweblogic/">Marcus (netweblogic)</a>, <a href="https://profiles.wordpress.org/imath/">Mathieu Viet (imath)</a>, <a href="https://github.com/bhoot-biswas">Mithun Biswas</a>, <a href="https://profiles.wordpress.org/modemlooper/">modemlooper</a>, <a href="https://profiles.wordpress.org/DJPaul/">Paul Gibbs (DJPaul)</a>, <a href="https://profiles.wordpress.org/r-a-y/">r-a-y</a>, <a href="https://profiles.wordpress.org/razor90/">razor90</a>, <a href="https://profiles.wordpress.org/espellcaste/">Renato Alves (espellcaste)</a>, <a href="https://profiles.wordpress.org/slaFFik/">Slava Abakumov (slaFFik)</a>, <a href="https://profiles.wordpress.org/netweb/">Stephen Edgar (netweb)</a>, <a href="https://profiles.wordpress.org/truchot/">truchot</a>, <a href="https://profiles.wordpress.org/venutius/">Venutius</a>, <a href="https://profiles.wordpress.org/wegosi/">wegosi</a>, and of course you for using BuddyPress <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f609.png" alt="😉" class="wp-smiley" /></p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-format-chat"></span></div>



<div class="wp-block-spacer"></div>



<h2>Feedbacks welcome!</h2>



<p>Receiving your feedback and suggestions for future versions of BuddyPress genuinely motivates and encourages our contributors. Please share&nbsp;your&nbsp;feedback about this version of BuddyPress in the comments area of this post. And of course, if you&#8217;ve found a bug: please tell us about it into our <a href="https://buddypress.org/support/">Support forums</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 28 Sep 2019 11:07:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"imath";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:28:"Gary: Talking with WP&amp;UP";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:25:"https://pento.net/?p=5120";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:47:"https://pento.net/2019/09/26/talking-with-wpup/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:348:"<p>At WordCamp Europe this year, I had the opportunity to chat with the folks at WP&amp;UP, who are doing wonderful work providing mental health support in the WordPress community.</p>



<p><a href="https://wpandup.org/podcast/getting-to-the-core-of-wordpress-021/">Listen to the podcast</a>, and check out the services that WP&amp;UP provide!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 26 Sep 2019 04:35:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Gary";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:94:"Post Status: Salesforce Ventures invests $300 million in Automattic, at a $3 billion valuation";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=68901";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://poststatus.com/salesforce-ventures-automattic/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6125:"<p>Salesforce Ventures is the investment arm of Salesforce. Prior to this investment in <a href="https://automattic.com/">Automattic</a>, Salesforce Ventures had <a href="https://www.salesforce.com/company/ventures/funds/">announced</a> $875 million raised across 11 fund initiatives — but none that amounts to $300 million. Previosuly, each fund has been between $50 to $125 million spread across several <a href="https://www.crunchbase.com/organization/salesforce-ventures/investments/investments_list">investments</a>.</p>



<p>I believe <a href="https://www.salesforce.com/company/ventures/">Salesforce Ventures</a> called up funds specifically for this strategic investment in Automattic, which would likely put their total dollars invested (or committed to existing funds) well beyond $1 billion, and the $300 million into Automattic would be their largest investment to date, to my knowledge.</p>



<p>Salesforce Ventures states on their website that they are exclusively seeking investments in &#8220;enterprise cloud&#8221; companies. In Automattic Founder and CEO Matt Mullenweg&#8217;s <a href="https://ma.tt/2019/09/series-d/">announcement</a> about the funding, he specifically noted how Salesforce CEO Marc Benioff, &#8220;helped open my eyes to the incredible traction WordPress <a href="https://wpvip.com/">and WP VIP</a> has seen in the enterprise market, and how much potential there still is there.&#8221; I am curious to see how Automattic changes their approach to VIP in particular, in light of this.</p>



<p>$300 million is a lot of money. Salesforce is joining Insight Venture Partners, Tiger Global, and True Ventures as primary outside investors in Automattic. </p>



<p>Given that Salesforce was the lead and only investor here, they now own a significant stake in Automattic — and it will be interesting to see what kind of confluence that enables between the two companies. Automattic CEO Matt Mullenweg tells me, &#8220;Automattic has been a long-time customer of Salesforce’s products, and we think there are lots of opportunities for closer integration.&#8221;</p>



<p>Since Automattic recently acquired Tumblr and brought on a few hundred new employees from it, it&#8217;s natural to think the new fundraising is related. I asked Matt about that, and he said it was unrelated in terms of financial justification, but they did, &#8220;disclose the Tumblr transaction to Salesforce during [their] discussions.&#8221;</p>



<p>Automattic hasn&#8217;t raised money since 2014, and it seems like this round is similar to prior ones, wherein the money helps spur their growth plans along but that they are operationally profitable — or close to it. Matt <a href="https://techcrunch.com/2019/09/19/automattic-raises-300-million-at-3-billion-valuation-from-salesforce-ventures/">told Techcrunch</a>, &#8220;The roadmap is the same. I just think we might be able to do it in five years instead of ten.&#8221;</p>



<p>Matt called the investment proof of Salesforce&#8217;s own &#8220;tremendous vote of confidence for Automattic and for the open web.&#8221; Salesforce does have some history of supporting <a href="https://opensource.salesforce.com/">open source projects</a>, although that shouldn&#8217;t be equated to an investment in Automattic as a company; it is a vote of confidence for companies that rely on open-source platforms as a part of their line of business.</p>



<p>Automattic is the single most significant contributor to WordPress and related open-source projects. It also relies on open-source software for its product development — particularly Jetpack and WooCommerce — and features like Gutenberg as the core experience for writing and site-building. How that blend of open source software and business development plays out, in the long run, is certainly of high interest to the open-source community.</p>



<p>I have long discussed on various platforms how I think there are a handful of companies that are big enough to acquire Automattic someday. I still think Automattic is more likely to go public at some point, but if they are acquired, Salesforce is definitely one of those few with the resources to make it happen, and perhaps the operational congruence as well.</p>



<p>Reaching a $3 billion valuation is an amazing feat that Automattic has achieved. Matt has said before that he believes each of their primary lines of business — WordPress.com, WooCommerce, and Jetpack — can be multi-billion dollar opportunities. I agree with him, particularly for WooCommerce. I think there&#8217;s a good chance WooCommerce will end up several times more valuable than all their other lines of business combined. <span class="pullquote alignleft">I would love to see these new funds be funnelled into the incredible opportunity in the eCommerce landscape; one only needs to look at what Shopify has done to see what&#8217;s possible, and just how successful it can be.</span> </p>



<p>I asked Matt why he was attracted to an investment from Salesforce&#8217;s VC arm, rather than an investment-only style firm. He said, &#8220;I love Salesforce’s philosophy, how they cultivate such a fantastic community, how they support progressive policies in San Francisco and the other cities they operate in, how they’ve been founder-led, their scale, their leadership, and their longevity in defining an entirely new class of software and being the most significant player in it.&#8221;</p>



<p>I love the point about Salesforce defining a class of software — and I have realized recently just how huge their developer community is — so I really appreciate this comment from Matt. Making commercial and SaaS software is a well-established business now. Automattic is in a unique position as the most powerful player in an <em>open</em> ecosystem proud of its independence. This provides many unique opportunities for Automattic the business and unique challenges for Automattic the WordPress community member.</p>



<p><em>Image credit: <a href="https://ma.tt/2019/09/series-d/">Matt Mullenweg</a>, whose blog I brazenly stole it from.</em></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 19 Sep 2019 23:05:39 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Brian Krogsgard";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:75:"HeroPress: Life Stacks Up –  From A Small Town Boy To A Geek Entrepreneur";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:56:"https://heropress.com/?post_type=heropress-essays&p=2963";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:192:"https://heropress.com/essays/life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur/#utm_source=rss&utm_medium=rss&utm_campaign=life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:10001:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2019/09/091719-min-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull Quote: For me, WordPress means freedom, self expression, and adaptability." /><p>A six year old is in deep thought. His gaze stuck on an intricate structure made with wooden sticks – a large rectangular box in the centre, a tall stick, some knitting threads running up and down. All this is arranged in a shelf in a common terrace wall of two middle class Indian homes.</p>
<p>The boy is holding what seems like a paper cup telephone – two paper cups with a thread running between. Soon, he smiles and throws one paper cup over the wall to the other side. His counterpart on the other side picks up his side of the &#8220;telephone&#8221; and they start talking.</p>
<p>&#8220;I made a TV using the sticks. I&#8217;m now going to set up a power line&#8230;&#8221;</p>
<p>&#8220;Awesome, I&#8217;ll be there after my homework!&#8221;</p>
<p>Aha! Now it makes sense. The kids are pretend-playing, and this one in particular is into science and model making. He has made an elaborate television model with limited resources.</p>
<p>Fast forward six years, and the boy is writing programs on a school computer. Couple years later he’s making model rockets and planes.</p>
<p>Fast forward another six years, and the boy is sitting with Bill Gates, being one of the eight national winners in a competition.</p>
<p>He goes on to launch India&#8217;s first electronic magazine, a web solutions business, local language versions of Linux and OpenOffice, a content management system, few books and a string of businesses that have made millions of dollars.</p>
<p>And he fondly remembers meeting Matt Mullenweg and Chris Lema at WordCamp San Francisco in 2014. His web agency business had gone bust around 2011, and his WordPress plugins business was picking up. Those meetings strengthened his conviction for WordPress and he doubled down on his plugins. Today his team takes care of 200,000+ active users across two dozen of their plugins – both free and premium.</p>
<p>That small town boy is me.</p>
<h3>Who I Am</h3>
<p>My name is Nirav Mehta. I live in Mumbai, and I&#8217;m super passionate content, commerce and contribution. I run three businesses – two in WordPress (<a href="https://www.storeapps.org/">StoreApps.org</a> – where we solve problems for growing WooCommerce stores,<a href="http://icegram.com/"> Icegram.com</a> – where creators find tools to inspire, engage and convert their audiences), and one SaaS business (<a href="https://www.putler.com/">Putler</a> – meaningful analytics for e-commerce).</p>
<p>I have done some or other form of writing for over two decades. I&#8217;ve done open source for my whole life and used Drupal and Joomla earlier. As a matter of fact, I created a content management system using PHP back in 2000. But I liked the simplicity and community of WordPress. So when I wanted to start two blogs in 2006, I jumped on to WordPress.</p>
<blockquote><p>And it was amazing. WordPress simplified a whole lot of things, allowed customization and had extensive plugin ecosystem.</p></blockquote>
<p>I continued blogging and tinkering with WordPress. WordPress kept growing, and when I was looking for &#8220;the next big thing&#8221; around 2011, I figured I can bet on e-commerce with WordPress.</p>
<p>There was no WooCommerce back then, and we built an extension to WPeC – an e-commerce plugin that was popular at that time. Smart Manager – the plugin we built – allowed managing products, orders and customers using an easy spreadsheet like interface. It quickly became popular. When WooCommerce came along, we ported our WPeC plugins to WooCommerce, and also became an official third-party developers with our Smart Coupons plugin. StoreApps – our WooCommerce plugins business continues to be our top business today.</p>
<p>WordPress has changed my life. For me, WordPress means freedom, self expression and adaptability.</p>
<h3>Where I Came From</h3>
<p>I&#8217;m from a small town, I am not an engineer, I didn&#8217;t do an MBA. I don&#8217;t have a godfather. But I&#8217;ve always wanted to contribute to a larger community, I&#8217;m a stickler for elegant solutions that solve practical problems and I&#8217;m ready to delay gratification. I believe grit and humility are essential. I&#8217;m a curious lifetime learner. I&#8217;ve realized that money is important, it&#8217;s a great resource. But I&#8217;ve also learnt that the joy of seeing someone benefit from your work far surpasses anything else.</p>
<p>WordPress fits perfectly here. It gives me a platform to reach out to the whole world. It&#8217;s built on community and greater good. There are lots of opportunities and entry barriers are low.</p>
<h3>What WordPress Has Given Me</h3>
<p>WordPress allowed me to exercise my creative skills and easily build solutions on top of the core platform. I am not a great marketer, and WordPress and WooCommerce enabled me to build strong businesses by tapping into their distribution prowess. WordPress was easy to learn, so when we found people with the right mindset, they became productive soon.</p>
<p>Icegram – our onsite and email marketing plugins business – is a clear example of the power of WordPress. Icegram Engage shows popups, header-footer bars and many other types of visitor engagement and call to action messages. Maintaining such a solution on a large scale would require huge server costs and sys-op efforts. We could avoid all that because we could keep most of the functionality in WordPress. It also provided a cleaner and much better user experience than typical SaaS solutions. When I wrote the initial code for it, I wanted to keep the frontend logic in JavaScript – and of course, WordPress allowed doing that. Eventually, it was also easy to migrate to a hybrid model – where complex functions are performed on our servers and rest remains in WordPress.</p>
<blockquote><p>WordPress has given me great friends. I&#8217;ve met so many talented people online and at WordCamps! Me and my WordPress friends have done amazing adventures together! And the circle keeps expanding. You will find amazing people in WordPress!</p></blockquote>
<p>When you look at my life, and if important events were plotted as a chart, you won&#8217;t see a straight curve. It&#8217;s a bundle of long lull-times with gyrating ups and downs in between. I studied behavior patterns, data modelling and visualization for Putler – our multi-system analytics solution for online businesses. I also get to see numbers from many other businesses. I wanted to analyze how businesses work. What causes success.</p>
<p>And one big, common takeaway &#8211; in both business and life &#8211; is that results are non-linear. There is no single cause to any result.</p>
<h3>Back To You</h3>
<p>It all starts simple. What you do today, is shaped by something you did earlier, and will shape something else you&#8217;ll do in the future.</p>
<p>Every little act of courage, every little getting out of your comfort zone, every new thing you learn, every setback, every little success&#8230; It all keeps building who you are.</p>
<p>You see, life stacks up!</p>
<p>Do not despair, do not lose faith. Series of actions produce a result, and you have the ability to act.</p>
<p>So stay on!</p>
<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur" class="rtsocial-twitter-button" href="https://twitter.com/share?text=Life%20Stacks%20Up%20%2D%20%20From%20A%20Small%20Town%20Boy%20To%20A%20Geek%20Entrepreneur&via=heropress&url=https%3A%2F%2Fheropress.com%2Fessays%2Flife-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fessays%2Flife-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fessays%2Flife-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur%2F&title=Life+Stacks+Up+%26%238211%3B++From+A+Small+Town+Boy+To+A+Geek+Entrepreneur" rel="nofollow" target="_blank" title="Share: Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/essays/life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur/&media=https://heropress.com/wp-content/uploads/2019/09/091719-min-150x150.jpg&description=Life Stacks Up -  From A Small Town Boy To A Geek Entrepreneur" rel="nofollow" target="_blank" title="Pin: Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/essays/life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur/" title="Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/essays/life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur/">Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 18 Sep 2019 03:00:17 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Nirav Mehta";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"Donncha: WP Super Cache 1.6.9: security update";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"https://odd.blog/?p=89502593";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:65:"https://odd.blog/2019/07/25/wp-super-cache-1-6-9-security-update/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1776:"<p><a href="https://wordpress.org/plugins/wp-super-cache/">WP Super Cache</a> is a full page caching plugin for WordPress.</p>



<p>Version 1.6.9 has just been released and is a required upgrade for all users as it resolves a security issue in the debug log. The issue can only be exploited if debugging is enabled in the plugin which will not be the case for almost all users.</p>



<p>The debug log is usually only enabled temporarily if a site owner is debugging a caching problem and isn&#8217;t something that should be left on permanently as it will slow down a site.</p>



<p>If there is an existing debug log it will be deleted after updating the plugin.</p>



<p>This release also improves the debug log by hiding sensitive data such as the ABSPATH  directory of the WordPress install and login cookies. Unfortunately in the past users have copied the log file data into forum posts. A warning message has been added asking the site owner not to publish the debug log.</p>



<p>Details of the security issue will be added to this post in time to allow sites to update their plugin.</p>

<p><strong>Related Posts</strong><ul><li> <a href="https://odd.blog/2013/10/23/wp-super-cache-1-4/" rel="bookmark" title="Permanent Link: WP Super Cache 1.4">WP Super Cache 1.4</a></li><li> <a href="https://odd.blog/2008/10/24/wp-super-cache-084-the-garbage-collector/" rel="bookmark" title="Permanent Link: WP Super Cache 0.8.4, the garbage collector">WP Super Cache 0.8.4, the garbage collector</a></li><li> <a href="https://odd.blog/2009/01/09/wp-super-cache-087/" rel="bookmark" title="Permanent Link: WP Super Cache 0.8.7">WP Super Cache 0.8.7</a></li></ul></p>
<p><a href="https://odd.blog/2019/07/25/wp-super-cache-1-6-9-security-update/" rel="nofollow">Source</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 25 Jul 2019 12:57:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"Donncha";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:71:"Akismet: Version 4.1.2 of the Akismet WordPress Plugin is Now Available";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"http://blog.akismet.com/?p=2039";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://blog.akismet.com/2019/05/14/akismet-plugin-4-1-2/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:885:"<p>Version 4.1.2 of <a href="http://wordpress.org/plugins/akismet/">the Akismet plugin for WordPress</a> is now available. It contains the following changes:</p>



<ul>
<li>We&#8217;ve reduced the number of API requests made by the plugin when attempting to verify the API key.</li>
<li>We&#8217;re now including additional data in the pingback pre-check API request to help make stats more accurate.</li>
<li>We fixed a bug that was enabling the &#8220;Check for Spam&#8221; button when no comments were eligible to be checked.</li>
<li>We&#8217;ve improved Akismet&#8217;s AMP compatibility.</li>
</ul>



<p>To upgrade, visit the Updates page of your WordPress dashboard and follow the instructions. If you need to download the plugin zip file directly, links to all versions are available in <a href="http://wordpress.org/plugins/akismet/">the WordPress plugins directory</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 14 May 2019 15:06:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"Christopher Finke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:31:"Gary: React Isn’t The Problem";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:25:"https://pento.net/?p=5045";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"https://pento.net/2019/04/04/react-isnt-the-problem/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6365:"<p>As React (via Gutenberg) becomes more present in the WordPress world, I&#8217;m seeing some common themes pop up in conversations about it. I spoke a bit about this kind of thing at WordCamp US last year, but if you don&#8217;t feel like sitting through a half hour video, let me summarise my thoughts. <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f642.png" alt="🙂" class="wp-smiley" /></p>



<div class="wp-block-embed__wrapper">

</div>



<p>I agree that React is hard. I <em>strongly</em> disagree with the commonly contrasted view that HTML, CSS, PHP, or vanilla JavaScript are easy. They&#8217;re all just as hard to work with as React, sometimes more-so, particularly when having to deal with the exciting world of cross-browser compatibility.</p>



<p>The advantage that PHP has over modern JavaScript development isn&#8217;t that it&#8217;s easy, or that the tooling is better, or more reliable, or anything like that. The advantage is that it&#8217;s familiar. If you&#8217;re new to web development, React is just as easy anything else to start with.</p>



<div class="wp-block-embed__wrapper">
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Something I’m seeing a lot.<br /><br />Beginners: This makes sense, we understand it.<br /><br />Experienced developers: There’s no way beginners would understand it. It’s not like &lt;a thing I learned ten years ago&gt;.</p>&mdash; Dan Abramov (@dan_abramov) <a href="https://twitter.com/dan_abramov/status/1096784072943300608?ref_src=twsrc%5Etfw">February 16, 2019</a></blockquote>
</div>



<p>I&#8217;m honestly shocked when someone manages to wade through the mess of tooling (even pre-Gutenberg) to contribute to WordPress. It&#8217;s such an incomprehensible, thankless, unreliable process, the tenacity of anyone who makes it out the other side should be applauded. That said, this high barrier is unacceptable.</p>



<p>I&#8217;ve been working in this industry for long enough to have forgotten the number of iterations of my personal development environment I&#8217;ve gone through, to get to where I can set up something for myself which <em>isn&#8217;t awful</em>. React wasn&#8217;t around for all of that time, so that can&#8217;t be the reason web development has been hard for as long as I remember. What is, then?</p>



<h2>Doing Better</h2>



<p>Over the past year or so, I&#8217;ve been tinkering with a tool to help deal with the difficulties of contributing to WordPress. That tool is called <a href="https://github.com/pento/testpress/">TestPress</a>, it&#8217;s getting pretty close to being usable, at least on MacOS. Windows support is a little less reliable, but getting better. <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f642.png" alt="🙂" class="wp-smiley" /> If you enjoy tinkering with tools, too, you&#8217;re welcome to try out the development version, but it does still has some bugs in it. Feedback and PRs are always welcome! There are some screenshots in <a href="https://github.com/pento/testpress/issues/114">this issue</a> that give an idea of what the experience is like, if you&#8217;d like to check it out that way.</p>



<p>TestPress is not a panacea: at best, it&#8217;s an attempt at levelling the playing field a little bit. You shouldn&#8217;t need years of experience to build a reliable development environment, that should be the bare minimum we provide.</p>



<h2>React is part of the solution</h2>



<p>There&#8217;s still a lot of work to do to make web development something that anyone can easily get into. I think React is part of the solution to this, however.</p>



<p>React isn&#8217;t without its problems, of course. Modern JavaScript can encourage iteration for the sake of iteration. Certainly, there&#8217;s a drive to React-ify All The Things (a trap I&#8217;m guilty of falling into, as well). React&#8217;s development model is fundamentally different to that of vanilla JavaScript or jQuery, which is why it can seem incomprehensible if you&#8217;re already well versed in the old way of doing things: it requires a shift in your mental model of how JavaScript works. This is a hard problem to solve, but it&#8217;s not insurmountable.</p>



<p>Perhaps a little controversially, I <em>don&#8217;t</em> think that React is guilty of causing the web to become less accessible. At worst, it&#8217;s continuing the long standing practice of web standards making accessibility an optional extra. Building anything beyond a basic, non-interactive web page with just HTML and CSS will inevitably cause accessibility issues, unless you happen to be familiar with the mystical combinations of accessible tags, or applying aria attributes, or styling your content in just the right way (and none of the wrong ways).</p>



<p>React (or any component-based development system, really) can improve accessibility for everyone, and we&#8217;re seeing this with Gutenberg already. By providing a set of base components for plugin and theme authors to use, we can ensure the correct HTML is produced for screen readers to work with. Much like desktop and mobile app developers don&#8217;t need to do anything to make their apps accessible (because it&#8217;s baked into the APIs they use to build their apps), web developers should have the same experience, regardless of the complexity of the app they&#8217;re building.</p>



<p>Arguing that accessibility needs to be part of the design process is the wrong argument. Accessibility shouldn&#8217;t be a consideration, it should be unavoidable.</p>



<h2>Do Better</h2>



<p>Now, can we do better? Absolutely. There&#8217;s always room for improvement. People shouldn&#8217;t need to learn React if they don&#8217;t want to. They shouldn&#8217;t have to deal with the complexities of the WCAG. They should have the freedom to tinker, and the reassurance that they can tinker without breaking everything.</p>



<p>The pre-React web didn&#8217;t arrive in its final form, all clean, shiny, and perfect. It took decades of evolution to get there. The post-React web needs some time to evolve, too, but it has the benefit of hindsight: we can compress the decades of evolving into a much shorter time period, provide a fresh start for those who want it, while also providing backwards compatibility with the existing ways of doing things.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 04 Apr 2019 06:31:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Gary";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:29:"Donncha: WP Super Cache 1.6.3";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"https://odd.blog/?p=89502017";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:49:"https://odd.blog/2018/08/17/wp-super-cache-1-6-3/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4917:"<p>WP Super Cache is a full page caching plugin for WordPress. When a page is cached almost all of WordPress is skipped and the page is sent to the browser with the minimum amount of code executed. This makes the page load much faster.</p>



<p>1.6.3 is the latest release and is mostly a bugfix release but it also adds some new features.</p>



<ul><li>Added cookie helper functions (<a href="https://github.com/Automattic/wp-super-cache/pull/580">#580</a>)</li><li>Added plugin helper functions (<a href="https://github.com/Automattic/wp-super-cache/pull/574">#574</a>)</li><li>Added actions to modify cookie and plugin lists. (<a href="https://github.com/Automattic/wp-super-cache/pull/582">#582</a>)</li><li>Really disable garbage collection when timeout = 0 (<a href="https://github.com/Automattic/wp-super-cache/pull/571">#571</a>)</li><li>Added warnings about DISABLE_WP_CRON (<a href="https://github.com/Automattic/wp-super-cache/pull/575">#575</a>)</li><li>Don&#8217;t clean expired cache files after preload if garbage collection is disabled (<a href="https://github.com/Automattic/wp-super-cache/pull/572">#572</a>)</li><li>On preload, if deleting a post don&#8217;t delete the sub directories if it&#8217;s the homepage. (<a href="https://github.com/Automattic/wp-super-cache/pull/573">#573</a>)</li><li>Fix generation of semaphores when using WP CLI (<a href="https://github.com/Automattic/wp-super-cache/pull/576">#576</a>)</li><li>Fix deleting from the admin bar (<a href="https://github.com/Automattic/wp-super-cache/pull/578">#578</a>)</li><li>Avoid a strpos() warning. (<a href="https://github.com/Automattic/wp-super-cache/pull/579">#579</a>)</li><li>Improve deleting of cache in edit/delete/publish actions (<a href="https://github.com/Automattic/wp-super-cache/pull/577">#577</a>)</li><li>Fixes to headers code (<a href="https://github.com/Automattic/wp-super-cache/pull/496">#496</a>)</li></ul>



<p>This release makes it much easier for plugin developers to interact with WP Super Cache. In the past a file had to be placed in the &#8220;WP Super Cache plugins directory&#8221; so that it would be loaded correctly but in this release I&#8217;ve added new actions that will allow you to load code from other directories too.</p>



<p>Use the <strong>wpsc_add_plugin</strong> action to add your plugin to a list loaded by WP Super Cache. Use it like this:<br /></p>



<pre class="wp-block-preformatted">do_action( 'wpsc_add_plugin', WP_PLUGIN_DIR . '/wpsc.php' )</pre>



<p>You can give it the full path, with or without ABSPATH. Use it after &#8220;init&#8221;. It only needs to be called once, but duplicates will not be stored.</p>



<p>In a similar fashion, use <strong>wpsc_delete_plugin</strong> to remove a plugin.</p>



<p>The release also makes it much simpler to modify the cookies used by WP Super Cache to identify &#8220;known users&#8221;. This is useful to identify particular types of pages such as translated pages that should only be shown to certain users. For example, visitors who have the English cookie will be shown cached pages in English. The German cookie will fetch German cached pages. The action <strong>wpsc_add_cookie</strong> makes this possible.</p>



<pre class="wp-block-preformatted">do_action( 'wpsc_add_cookie', 'language' );</pre>



<p>Execute that in your plugin and WP Super Cache will watch out for the language cookie. The plugin will use the cookie name <em>and</em> value in determining what cached page to display. So &#8220;language = irish&#8221; will show a different page to &#8220;language = french&#8221;.</p>



<p>Use <strong>wpsc_delete_cookie</strong> to remove a cookie. Cache files won&#8217;t be deleted. It&#8217;s doubtful they&#8217;d be served however because of the hashed key used to name the filenames.<br /></p>



<pre class="wp-block-preformatted">do_action( 'wpsc_delete_cookie', 'language' );</pre>



<p>If you&#8217;re going to use either of the plugin or cookie actions here I recommend using <strong>Simple Caching</strong>. While the plugin will attempt to update mod_rewrite rules, it is much simpler to have PHP serve the files. Apart from that, any plugins loaded by WP Super Cache will be completely skipped if Expert mode is enabled.</p>

<p><strong>Related Posts</strong><ul><li> <a href="https://odd.blog/2008/10/24/wp-super-cache-084-the-garbage-collector/" rel="bookmark" title="Permanent Link: WP Super Cache 0.8.4, the garbage collector">WP Super Cache 0.8.4, the garbage collector</a></li><li> <a href="https://odd.blog/2009/01/09/wp-super-cache-087/" rel="bookmark" title="Permanent Link: WP Super Cache 0.8.7">WP Super Cache 0.8.7</a></li><li> <a href="https://odd.blog/2010/02/08/wp-super-cache-099/" rel="bookmark" title="Permanent Link: WP Super Cache 0.9.9">WP Super Cache 0.9.9</a></li></ul></p>
<p><a href="https://odd.blog/2018/08/17/wp-super-cache-1-6-3/" rel="nofollow">Source</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 17 Aug 2018 16:36:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"Donncha";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:32:"Mark Jaquith: Page Links To v3.0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:40:"http://markjaquith.wordpress.com/?p=5675";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"https://markjaquith.wordpress.com/2018/07/23/page-links-to/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2482:"<p>Today I pushed an update to my redirect and repointing plugin, <a href="https://wordpress.org/plugins/page-links-to/">Page Links To</a>. Tomorrow, this plugin will have been in the WordPress.org Plugin Directory for 13 years (it was the 339th plugin in the WordPress plugin repository; there are now over 75,000!).</p>
<p>To celebrate its transition to a teenager, I&#8217;ve added some new features and UI enhancements.</p>
<p>Last month, I received survey responses from over 800 Page Links To users and learned a lot about how it&#8217;s being put to work. One of the most interesting things I found was how many people are using it for URL redirects. For example, they might have a really long URL on their own site or someone else&#8217;s site that they want to be nice. <em>example.com/summer-sale</em> instead of <em>example.com/store/specials.aspx?season=summer&amp;_utm_source=internal</em>. But in order to create these redirects, you have to go through the cluttered and sometimes slow post creation screen. All you really need to create a redirect is a title, a destination URL, and a local short URL.</p>
<p>You&#8217;ll now find a menu item &#8220;Add Page Link&#8221; that will allow you to quickly add a redirected Page without having to wait for the entire WordPress post editing interface to load. It&#8217;s <em>super</em> fast, and it doesn&#8217;t redirect you away from the screen you&#8217;re on.</p>
<p><img /></p>
<p>Since short URLs are better for sharing (and remembering), the UI will give you a little push to shorten the URL if the one generated from your title is too long. From there, you can Save Draft or Publish.</p>
<img />Hey, that URL is getting a bit long
<img />Custom slug, for a better short URL
<p>Additionally, this release includes a &#8220;link&#8221; indicator on post and page list screens, so you can easily see what items have been re-pointed with Page Links To. When hovered, the link icon will reveal the destination URL for a quick view.</p>
<img />The &#8220;link&#8221; icon means that this item has been pointed elsewhere.
<p>If you want to grab the &#8220;local&#8221; short URL (which will be redirected to your chosen URL when someone visits it), just click &#8220;Copy Short URL&#8221; from the actions, and it&#8217;ll be in your clipboard.</p>
<img />Hover the &#8220;link&#8221; icon to see where it&#8217;s pointing.
<p>That&#8217;s it for version 3.0, but I&#8217;ll have more to announce soon — stay tuned!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Jul 2018 22:02:18 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Mark Jaquith";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:20;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"Mark Jaquith: Making ScoutDocs: Build Tools";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:40:"http://markjaquith.wordpress.com/?p=5665";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:74:"https://markjaquith.wordpress.com/2018/06/19/making-scoutdocs-build-tools/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2928:"<p>Continuing my series about <a href="https://wordpress.org/plugins/scoutdocs/">ScoutDocs</a> and the process of building it, this week I&#8217;m talking about <strong>Build Tools</strong>.</p>
<p><img /><em>What is <a href="https://scoutdocs.com/">ScoutDocs</a>? ScoutDocs is a WordPress plugin that adds simple file-sharing to your WordPress site.</em></p>
<p>Coding in React involves JSX, a bizarre-but-wonderful XML syntax that you dump directly into the middle of your JavaScript code. It feels exquisitely wrong. Browsers agree, so your JSX-containing JS code will have to be transpiled to regular JavaScript. This can involve using a complex maze of tools. Babel, NPM, Webpack, Browserify, Gulp, Grunt, Uglify, Uglifyify (yes, you read that right), and more. You have decisions to make, and you will find fierce advocates for various solutions.</p>
<p>For ScoutDocs, I decided to go with <a href="https://gruntjs.com/">Grunt</a> for task running, because I was already comfortable with it, and I needed it for <a href="https://www.npmjs.com/package/grunt-wp-deployhttps://www.npmjs.com/package/grunt-wp-deploy">grunt-wp-deploy</a>. <b>Use a task runner you are already comfortable with.</b> Even if it is just NPM scripts. You’re learning a lot of new things already. It’s okay to keep your task runner setup.</p>
<p>Next, I had to choose a JS bundler which would let me write and use modular code that gets pulled together into a browser-executable bundle. After deliberating between <a href="https://webpack.js.org">Webpack</a> and <a href="http://browserify.org/">Browserify</a>, I chose Browserify. <b>Webpack is really complicated.</b> It is also very powerful. I recommend you avoid it until you need it. I haven’t needed it yet, and found Browserify to be easier to configure and use, even though it’s a bit on the slow side.</p>
<p>As I was building ScoutDocs and tweaking my dev tools, tweaking my Grunt file, and writing code to search/replace strings etc, I began to feel like the time I was spending too much time on tooling. Was I becoming one of those people who spend all their time listening to productivity podcasts instead of… being productive? I can see how someone could get sucked into that trap, but putting a reasonable amount of time into configuring your development tools can pay dividends for you beyond simply the time saved. It can also prevent mistakes, keep you in coding mode more often, and increasing your confidence in your code builds. <b>Spend the time up front to make your tools work for you.</b></p>
<p>Other posts in this series:</p>
<ul>
<li><a href="https://markjaquith.wordpress.com/2018/06/01/lessons-learned-making-scoutdocs-outsourcing/">Outsourcing</a></li>
<li><a href="https://markjaquith.wordpress.com/2018/06/11/making-scoutdocs-react">React</a></li>
<li>WordPress Rest API</li>
<li>PHP 7</li>
<li>Build tools</li>
<li>Unit testing</li>
</ul>
<p>&nbsp;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 19 Jun 2018 17:59:34 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Mark Jaquith";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:21;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"Lorelle on WP: Vulnerability in phpMyAdmin Requires Immediate Patch";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:37:"http://lorelle.wordpress.com/?p=14409";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:94:"https://lorelle.wordpress.com/2018/01/06/vulnerability-in-phpmyadmin-requires-immediate-patch/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4312:"<p>A <a href="http://www.itprotoday.com/patch-management/critical-csrf-security-vulnerability-phpmyadmin-database-tool-patched" title="Critical CSRF Vulnerability in phpMyAdmin Database Tool Patched | IT Pro">critical CSRF Vulnerability in phpMyAdmin Database administration tool</a> has been found and a <a href="https://www.phpmyadmin.net/security/PMASA-2017-9/" title="phpMyAdmin - Security - PMASA-2017-9">patch is available</a> for all computers and servers running the MySQL database. </p>
<p>Does this include you?</p>
<p>If you are using WordPress, yes it does. </p>
<p>Contact your web host to ensure phpMyAdmin is updated immediately. </p>
<p>If you are self-hosted and manage your own server, update phpMyAdmin immediately. </p>
<p>If you are using WordPress or phpMyAdmin and MySQL on your computer through <a href="http://www.wampserver.com/en/" title="WAMP">WAMP</a>, <a href="http://www.mamp.info/en/index.html" title="MAMP">MAMP</a>, <a href="http://www.apachefriends.org/en/xampp.html" title="XAMPP">XAMPP</a>, <a href="http://www.instantwp.com/" title="Instant WordPress">Instant WordPress</a>, <a href="http://serverpress.com/products/desktopserver/" title="DesktopServer">DesktopServer</a>, <a href="http://bitnami.org/stack/wordpress" title="BitNami">BitNami</a> or any of the other ways you can install WordPress on your computer or a stick (USB), update phpMyAdmin by using the patch or check the install technique&#8217;s site for updates. </p>
<p><strong>If you are using WordPress.com, don&#8217;t worry.</strong> This does not apply to you or your site. </p>
<p>The flaw affects phpMyAdmin versions 4.7.x prior to 4.7.7. Hopefully, your server/web host company has been updating phpMyAdmin all along and you don&#8217;t need to worry, but even though this is a medium security vulnerability, it is your responsibility as a site owner and administrator to ensure that your site is safe. Don&#8217;t just rely on GoDaddy, Dreamhost, or whatever hosting service you use to take care of these things for you. Sometimes they are on top of these before an announcement is made public. Other times, they are clueless and require customer intervention and nagging.<br />
<span id="more-14409"></span></p>
<p>Now, what is phpMyAdmin?</p>
<p>MySQL is an open source database program, and <a href="https://www.phpmyadmin.net/" title="phpMyAdmin">phpMyAdmin</a> is the free, open source tool that makes the administration and use of MySQL easier to manage. <em>It is not a database. It is a database manager.</em> You can <a href="https://lorelle.wordpress.com/2014/08/10/find-search-replace-and-delete-in-the-wordpress-database/" title="Find, Search, Replace, and Delete in the WordPress Database « Lorelle on WordPress">easily search and replace data</a> in the database, make changes, and do other maintenance and utility tasks in the database.</p>
<p>Every installation of WordPress requires PHP and MySQL along with a variety of other web-based programming packages and software. Most installations by web hosts and portable versions of WordPress add phpMyAdmin to manage the WordPress site. It is not required for WordPress to work, but don&#8217;t assume that it is or isn&#8217;t installed. CHECK. </p>
<p>To find out if phpMyAdmin is installed on your site:</p>
<ol>
<li>Check with your web host and ask. Don&#8217;t expect their customer service staff to know for sure. Make them check your account and verify whether or not it is installed, and if they&#8217;ve updated. Push them for a specific answer.</li>
<li>Check the site admin interface (cPanel, Plesk, etc.) to see if it is installed.</li>
<li>Log into your site through secure FTP into the root (if you have access) and look for the installation at <em>/usr/share/phpmyadmin</code> or <code>localhost/phpmyadmin</code></em>. Unfortunately, it could be anywhere depending upon the installation as these are virtual folders, not folders found on your computer, so it must be assigned to a location.</li>
<li>If running a portable installation of MySQL and/or WordPress, follow the instructions for that tool and download and install all patches to ensure phpMyAdmin is updated to the latest secure version.</li>
</ol>
<div class="sig">
<p><img src="https://lorelle.files.wordpress.com/2006/08/sig.gif" alt="" /></p>
<hr /></div>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 06 Jan 2018 16:55:40 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"Lorelle VanFossen";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:22;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"bbPress: bbPress 2.5.14";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://bbpress.org/?p=186831";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:48:"https://bbpress.org/blog/2017/09/bbpress-2-5-14/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:710:"<p>Today we are releasing bbPress 2.5.14, which fixes a few small bugs we&#8217;ve noticed since 2.5.13 was released, in particular we&#8217;ve fixed some incompatibilities when using PHP 7.1, an unexpected debug notice with the Topics &amp; Replies widgets, and improved validation and sanitization of database properties with the forum converter.</p>
<p>Also, remember that since bbPress 2.5.12, the minimum WordPress version allowed is 4.7. If you need to use a previous version of WordPress, you will want to continue to use 2.5.11.</p>
<p>bbPress 2.6 is still in the release candidate phase while we tie up some loose ends across WordPress.org, but I&#8217;ll let you know when it&#8217;s ready to go!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 12 Sep 2017 19:39:13 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Stephen Edgar";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:23;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"Lorelle on WP: WordPress School: Shortcodes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:37:"http://lorelle.wordpress.com/?p=14325";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"https://lorelle.wordpress.com/2017/08/18/wordpress-school-shortcodes/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:12915:"<p><a href="https://lorelle.wordpress.com/category/wordpress/wordpress-school/" title="WordPress School taught by Lorelle VanFossen."><img /></a></p>
<p>WordPress shortcodes are abbreviated code placed into the WordPress Visual or Text Editors that expands into a larger code structure. As we continue with <a href="https://lorelle.wordpress.com/classes-and-workshops/wordpress-school/" title="WordPress School « Lorelle on WordPress">Lorelle&#8217;s WordPress School free online course</a>, it&#8217;s time to explore the basics of WordPress shortcodes.</p>
<p>The following is the embed code for a Google Map, pointing to one of my favorite local museums, <a href="http://ricenorthwestmuseum.com/">The Rice Northwest Rocks and Minerals Museum</a> in Hillsboro, Oregon:</p>
<p><code>&lt;a href="https&#058;&#047;&#047;www&#046;google.com/maps/embed?pb=!1m18!1m12!1m3!1d2792.809130780463!2d-122.94987648443889!3d45.57427677910247!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54950456e76e254b%3A0xdfad5d11bde5b6cc!2s26385+NW+Groveland+Dr%2C+Hillsboro%2C+OR+97124!5e0!3m2!1sen!2sus!4v1502560000052"&gt;https&#058;&#047;&#047;www&#046;google.com/maps/embed?pb=!1m18!1m12!1m3!1d2792.809130780463!2d-122.94987648443889!3d45.57427677910247!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54950456e76e254b%3A0xdfad5d11bde5b6cc!2s26385+NW+Groveland+Dr%2C+Hillsboro%2C+OR+97124!5e0!3m2!1sen!2sus!4v1502560000052&lt;/a&gt;</code></p>
<p>When the post or Page is saved, WordPress.com automatically converts it to the embed code for Google Maps like this:</p>
<p><code>&#091;googlemaps https&#058;&#047;&#047;www&#046;google&#046;com/maps/embed?pb=!1m18!1m12!1m3!1d2792.809130780463!2d-122.94987648443889!3d45.57427677910247!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54950456e76e254b%3A0xdfad5d11bde5b6cc!2s26385+NW+Groveland+Dr%2C+Hillsboro%2C+OR+97124!5e0!3m2!1sen!2sus!4v1502560000052&amp;w=600&amp;h=450]</code></p>
<p>This is what you see in your Visual or Text/HTML editors. Doesn&#8217;t look like a map, yet, does it? </p>
<p>When the post is previewed or published, you will see the map like this:</p>
<div class="googlemaps"></div>
<p>The map is not a screenshot. It is interactive. Zoom in and out and move around on the map. The <a href="https://en.support.wordpress.com/google-maps/" title="Google Maps — Support — WordPress.com">Google Maps shortcode</a> taps into the Google Maps API allowing a live section of the map to be embedded on your site to help people find locations and directions. </p>
<p>Google Maps are a great way of providing instructions to the location of a store or company on a Contact web page. They are also fun to embed in a post about a favorite park, hike, fishing hole, vacation spot, or even create a custom map that charts your travels, hikes, or a specific route for shopping or exploring. </p>
<p><strong>NOTE:</strong> <em>Google Map embeds are tricky. You need to search for the exact address and use that embed code. If you search for a business name, you may get an invalid server request from Google Maps. Also note that WordPress.com has made it easier to use shortcodes by skipping the extra code and converting links and embed codes automatically to shortcodes. This may require saving your post as a draft twice before you can see the results on the front end preview of the post or Page.</em></p>
<p>Shortcodes allow the user to add content and functionality to a WordPress site without knowing extensive code or digging into the programming of a WordPress Theme or Plugin. With the shortcut of a shortcode, WordPress users may add all sorts of customization features to their site.</p>
<p>There are a variety of shortcodes in the core of WordPress. WordPress Themes have the ability to enable or disable these, and add more, as do WordPress Plugins.</p>
<p>Let&#8217;s experiment with the <a title="Archives Shortcode &mdash; Support &mdash; WordPress.com" href="http://en.support.wordpress.com/archives-shortcode/">Archives Shortcode</a>.</p>
<ol>
<li>Add a New Page to your site. Title it &#8220;Site Map&#8221; or &#8220;Archives.&#8221;</li>
<li>Type in <code>&#091;archives]</code>.</li>
<li>Preview, then publish the post when ready to see a listing of all of the published posts on your site in a list.</li>
</ol>
<p>Check out my <a href="https://lorelle.wordpress.com/site-map/" title="Site Map « Lorelle on WordPress">site map</a> as an example of what&#8217;s possible. </p>
<h3>What You Need to Know About WordPress Shortcodes</h3>
<p>Shortcodes come with WordPress out of the box, and also with WordPress Themes and Plugins. These snippets of code allow the user to add functionality to their site without touching the code. </p>
<p>The PHP code that enables the functionality, and adds the ability to use the abbreviated code to generate that functionality on the site, is called a <code>function</code>. </p>
<p>At its core, this is the function found to generate all WordPress Shortcodes:</p>
<pre class="brush: xml; title: ; notranslate">//[foobar]
function foobar_func( $atts ){
	return "foo and bar";
}
add_shortcode( 'foobar', 'foobar_func' );</pre>
<p>The attributes, represented in this abbreviated version by <code>$atts</code>, are the instructions as to what the shortcode is to do.</p>
<p>In the expanded form with functionality, I&#8217;ve called the shortcode &#8220;elephant&#8221; and set up two attribute values, &#8220;trumpet loudly&#8221; and &#8220;stomp.&#8221; </p>
<pre class="brush: xml; title: ; notranslate">// [elephant foo="foo-value"]
function elephant_func( $atts ) {
    $a = shortcode_atts( array(
        'foo' =&gt; 'trumpet loudly',
        'bar' =&gt; 'stomp',
    ), $atts );

    return "foo = {$a['foo']}";
}
add_shortcode( 'elephant', 'elephant_func' );</pre>
<p>Depending upon what &#8220;foo&#8221; and &#8220;bar&#8221; represent, the results would be &#8220;trumpet loudly&#8221; and &#8220;stomp.&#8221; What these represent are HTML code, modifications to HTML code, and initiates the programming such as generating a list of all the posts you&#8217;ve published as an archive list.</p>
<p>Right now, you aren&#8217;t at the stage where you can program shortcodes and add them to WordPress Themes or create WordPress Plugins, so I&#8217;m not going to dive into these much deeper. You need to learn how these work and how to use them on your site, and the more you use them, the better feel you will have for what a shortcode can do on your site. </p>
<p>WordPress.com offers a <a href="https://en.support.wordpress.com/category/shortcodes/" title="Support — WordPress.com">wide range of shortcodes</a> to add functionality to your site. To learn about how to use these, see <a title="Shortcodes &mdash; Support &mdash; WordPress.com" href="http://en.support.wordpress.com/shortcodes/">Shortcodes &mdash; Support</a>. </p>
<p>Here are some examples of shortcodes to experiment with on WordPress.com.</p>
<ul>
<li><a href="https://en.support.wordpress.com/videos/youtube/" title="YouTube — Support — WordPress.com">YouTube Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/audio/" title="Audio — Support — WordPress.com">Audio Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/soundcloud-audio-player/">SoundCloud Audio Player Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/gallery/" title="Galleries and Slideshows — Support — WordPress.com">Galleries and Slideshows Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/instagram/" title="Instagram — Support — WordPress.com">Instagram Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/archives-shortcode/" title="Create an Archive using the Archives Shortcode — Support — WordPress.com">Archives Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/blog-subscription-shortcode/" title="Blog Subscription Shortcode — Support — WordPress.com">Blog Subscription Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/forms/contact-form/" title="Contact Form — Support — WordPress.com">Contact Form Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/google-maps/" title="Google Maps — Support — WordPress.com">Google Maps Shortcode</a></li>
<li><a href="https://en.support.wordpress.com/recipes/" title="Recipes — Support — WordPress.com">Recipes Shortcode</a></li>
</ul>
<h4>More Information on WordPress Shortcodes</h4>
<ul>
<li><a title="Shortcodes &mdash; Support &mdash; WordPress.com" href="http://en.support.wordpress.com/shortcodes/">Shortcodes &#8211; Support &#8211; WordPress.com</a></li>
<li><a title="Shortcodes &mdash; Support &mdash; WordPress.com" href="http://en.support.wordpress.com/shortcodes/">List of Shortcodes available for WordPress.com sites</a></li>
<li><a title="Shortcode &laquo; WordPress Codex" href="http://codex.wordpress.org/Shortcode">Shortcode &#8211; WordPress Codex</a></li>
<li><a title="Support &mdash; WordPress.com" href="http://en.support.wordpress.com/category/shortcodes/">Shortcodes for WordPress.com</a></li>
<li><a title="Gallery Shortcode &laquo; WordPress Codex" href="http://codex.wordpress.org/Gallery_Shortcode">Gallery Shortcode &#8211; WordPress Codex</a></li>
<li><a href="https://www.smashingmagazine.com/2012/05/wordpress-shortcodes-complete-guide/" title="WordPress Shortcodes: A Complete Guide – Smashing Magazine">WordPress Shortcodes: A Complete Guide – Smashing Magazine</a></li>
<li><a href="http://www.wpbeginner.com/wp-tutorials/how-to-add-a-shortcode-in-wordpress/" title="How to Add A Shortcode in WordPress?">How to Add A Shortcode in WordPress? &#8211; WPBeginner</a></li>
<li><a href="http://www.wpbeginner.com/beginners-guide/7-essential-tips-for-using-shortcodes-in-wordpress/" title="7 Essential Tips for Using Shortcodes in WordPress">7 Essential Tips for Using Shortcodes in WordPress &#8211; WPBeginner</a></li>
<li><a href="https://code.tutsplus.com/articles/getting-started-with-wordpress-shortcodes--wp-21197" title="Getting Started With WordPress Shortcodes">Getting Started With WordPress Shortcodes &#8211; Envatotuts+</a></li>
</ul>
<h3>Assignment</h3>
<p><img />Your assignment in these WordPress School exercises is to experiment with WordPress shortcodes, specifically the ones available on WordPress.com. </p>
<p>I&#8217;ve listed some examples of shortcodes on WordPress.com above, and you may find more in the <a href="https://en.support.wordpress.com/category/shortcodes/" title="Support — WordPress.com">WordPress.com list of Shortcodes</a>.</p>
<p>Your assignment is to use shortcodes to add features to your site. </p>
<ul>
<li>Create a Page called &#8220;Site Map&#8221; or &#8220;Archives&#8221; and add an archive list shortcode.</li>
<li>Add a Google Map to a post or Page using the Google Maps shortcode.</li>
<li>Add a gallery to a post or Page with the gallery shortcode, testing the various options (parameters) to get the look and feel you like best.</li>
<li>Add a recipe to a post using the recipe shortcode.</li>
<li>Find another shortcode with a variety of features to experiment with. See how many ways you can change the look and feel of the content. If you wish, blog about your discoveries with screenshots or examples in the post. Let us know about it in the comments below so we can come inspect your work.</li>
</ul>
<p><em>This is a tutorial from <a href="https://lorelle.wordpress.com/classes-and-workshops/wordpress-school/" title="WordPress School « Lorelle on WordPress">Lorelle&#8217;s WordPress School</a>. For more information, and to join this free, year-long, online WordPress School, see:</em></p>
<ul>
<li><a href="https://lorelle.wordpress.com/2015/01/24/welcome-to-lorelles-wordpress-school/" title="Welcome to Lorelle’s WordPress School « Lorelle on WordPress">Lorelle&#8217;s WordPress School Introduction</a></li>
<li><a href="https://lorelle.wordpress.com/classes-and-workshops/wordpress-school/" title="WordPress School « Lorelle on WordPress">Lorelle&#8217;s WordPress School Description</a></li>
<li><a href="https://lorelle.wordpress.com/classes-and-workshops/wordpress-school/tutorials/" title="Tutorials « Lorelle on WordPress">WordPress School Tutorials List</a></li>
<li><a href="https://plus.google.com/u/0/communities/115251582756616355670" title="WordPress School Google+ Community">WordPress School Google+ Community</a></li>
<li><a href="https://lorelle.wordpress.com/2015/01/12/wordpress-publishing-checklist/" title="WordPress Publishing Checklist">WordPress Publishing Checklist</a></li>
<li><a href="https://lorelle.wordpress.com/classes-and-workshops/wordpress-school/feedback-and-criticism/" title="How to Give Feedback and Criticism - Lorelle's WordPress School.">How to Give Feedback and Criticism</a></li>
</ul>
<div class="sig">
<p><img src="https://lorelle.files.wordpress.com/2006/08/sig.gif" alt="" /></p>
<hr /></div>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 18 Aug 2017 11:02:13 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"Lorelle VanFossen";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:24;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"bbPress: bbPress 2.5.13";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://bbpress.org/?p=185643";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:48:"https://bbpress.org/blog/2017/07/bbpress-2-5-13/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:983:"<p>Today we are releasing bbPress 2.5.13, which fixes a few small bugs we&#8217;ve noticed since 2.5.12 was released, and also adds some sanitization to anonymous user data that went missing from previous versions.</p>
<p>If your site allows anonymous users (users without registered accounts) to create topics &amp; replies in your forums, you&#8217;ll want to upgrade to 2.5.13 right away. This feature is not very widely used on public forums because spammers very aggressively target these kinds of sites, but for communities that rely on this feature, please know you can safely upgrade to 2.5.13 without any issues.</p>
<p>Also, remember that since bbPress 2.5.12, the minimum WordPress version allowed is 4.7. If you need to use a previous version of WordPress, you will want to continue to use 2.5.11.</p>
<p>bbPress 2.6 is still in the release candidate phase while we tie up some loose ends across WordPress.org, but I&#8217;ll let you know when it&#8217;s ready to go!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 18 Jul 2017 20:17:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:17:"John James Jacoby";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:25;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:63:"WordPress.tv Blog: The Humanity Of WordPress – Rich Robinkoff";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"http://blog.wordpress.tv/?p=654";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:78:"https://blog.wordpress.tv/2016/09/23/the-humanity-of-wordpress-rich-robinkoff/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:922:"<p>Rich Robinkoff &#8220;nails it&#8221; during his presentation titled The Humanity of WordPress!</p>
<p>Rich gave this presentation at <a href="https://2016.columbus.wordcamp.org/speakers/">WordCamp Columbus</a> on August 27th and again at <a href="https://wordpress.tv/?s=pittsburgh">WordCamp Pittsburgh</a> on September 17th. I was lucky enough to be in attendance in Pittsburgh.</p>
<p>He talks about human interactions and the fact that people may not realize the impact they might have on somebodies life in just a short conversation. Rich gives several examples of the relationships that can be built and the giving nature of the WordPress Community.</p>
<p>Please watch until the end as Rich talks about the contributions to the WordPress Community by #WPMOM.</p>
<p></p>
<p>See more great WordCamp videos at <a href="https://wordpress.tv" target="_blank">WordPress.tv »</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 23 Sep 2016 17:14:34 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"John Parkinson";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:26;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:74:"WordPress.tv Blog: Data-Driven SEO with Google Analytics – Rebecca Haden";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"http://blog.wordpress.tv/?p=634";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:89:"https://blog.wordpress.tv/2016/09/10/data-driven-seo-with-google-analytics-rebecca-haden/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:691:"<div class="video-description">
<p>SEO can be confusing if you rely on tips and tricks. Instead, you can rely on data from your own website.This presentation by Rebecca Haden from  <a href="http://wordpress.tv/event/wordcamp-fayetteville-2016/">WordCamp Fayetteville 2016</a> helps you to get to know Google Analytics and other analytics tools with WordPress plugins, find the actionable information in your analytics reports, and implement your own SEO strategy.</p>
<p></p>
<p><a href="http://2016.fayetteville.wordcamp.org/files/2016/07/Data-Driven-SEO.ppt">Presentation Slides »</a></p>
<p>More great WordCamp videos on <a href="http://wordpress.tv/">WordPress.tv »</a></p>
</div>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 10 Sep 2016 00:16:08 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"Jerry B";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:27;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"WP Mobile Apps: WordPress for iOS: Version 6.4";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"http://apps.wordpress.com/?p=3568";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:68:"https://apps.wordpress.com/2016/08/26/wordpress-for-ios-version-6-4/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4146:"<p>Hi there, WordPress users! <a href="https://itunes.apple.com/us/app/wordpress/id335703880?mt=8&uo=6&at=&ct=">Version 6.4 of the WordPress for iOS app</a> is now available in the App Store.</p>
<h1>What&#8217;s New:</h1>
<p><strong>iPad Keyboard Shortcuts.</strong> Press down the command key on your external keyboard to see a list of available shortcuts in the main screen and in the post editor.</p>

<a href="https://apps.wordpress.com/img_0007/"><img width="300" height="225" src="https://apps.files.wordpress.com/2016/08/img_0007.png?w=300&h=225" class="attachment-medium size-medium" alt="" /></a>
<a href="https://apps.wordpress.com/img_0006/"><img width="300" height="225" src="https://apps.files.wordpress.com/2016/08/img_0006.png?w=300&h=225" class="attachment-medium size-medium" alt="" /></a>

<p><strong>Share Media.</strong> Our sharing extension now supports media, too!</p>

<a href="https://apps.wordpress.com/img_2385/"><img width="169" height="300" src="https://apps.files.wordpress.com/2016/08/img_2385.png?w=169&h=300" class="attachment-medium size-medium" alt="" /></a>
<a href="https://apps.wordpress.com/img_2386/"><img width="169" height="300" src="https://apps.files.wordpress.com/2016/08/img_2386.png?w=169&h=300" class="attachment-medium size-medium" alt="" /></a>

<p><strong>People Management.</strong> You can now manage your site&#8217;s users and roles using your mobile device.</p>

<a href="https://apps.wordpress.com/img_2392/"><img width="169" height="300" src="https://apps.files.wordpress.com/2016/08/img_2392.png?w=169&h=300" class="attachment-medium size-medium" alt="" /></a>
<a href="https://apps.wordpress.com/img_2393/"><img width="169" height="300" src="https://apps.files.wordpress.com/2016/08/img_2393.png?w=169&h=300" class="attachment-medium size-medium" alt="" /></a>
<a href="https://apps.wordpress.com/img_2394/"><img width="169" height="300" src="https://apps.files.wordpress.com/2016/08/img_2394.png?w=169&h=300" class="attachment-medium size-medium" alt="" /></a>

<p><strong>Search in the Reader.</strong> The Reader now has search capability and autocompletes suggestions.</p>

<a href="https://apps.wordpress.com/img_2390/"><img width="169" height="300" src="https://apps.files.wordpress.com/2016/08/img_23901.png?w=169&h=300" class="attachment-medium size-medium" alt="" /></a>
<a href="https://apps.wordpress.com/img_2389/"><img width="169" height="300" src="https://apps.files.wordpress.com/2016/08/img_2389.png?w=169&h=300" class="attachment-medium size-medium" alt="" /></a>

<p><strong>Improved Gestures.</strong> Full screen image previews can be dismissed with a swanky flick/toss gesture.</p>
<p><strong>Bugs Squashed.</strong> A new homemade bug spray formula has allowed us to squash <a href="https://github.com/wordpress-mobile/WordPress-iOS/issues?q=is%3Aclosed+is%3Aissue+milestone%3A6.4+label%3A%22%5BType%5D+Bug%22">many uninvited guests</a>.</p>
<p><strong>And much more! </strong>You can see the full list of changes <a href="https://github.com/wordpress-mobile/WordPress-iOS/issues?utf8=✓&q=is%3Aissue%20is%3Aclosed%20milestone%3A6.4">here</a>.</p>
<h1>Thank You</h1>
<p>Thanks to all of the contributors who worked on this release:<br />
<a href="https://github.com/aerych">@aerych</a>, <a href="https://github.com/astralbodies">@astralbodies</a>, <a href="https://github.com/claudiosmweb">@claudiosmweb</a>, <a href="https://github.com/diegoreymendez">@diegoreymendez</a>, <a href="https://github.com/frosty">@frosty</a>, <a href="https://github.com/jleandroperez">@jleandroperez</a>, <a href="https://github.com/koke">@koke</a>, <a href="https://github.com/kurzee">@kurzee</a>, <a href="https://github.com/kwonye">@kwonye</a>, <a href="https://github.com/oguzkocer">@oguzkocer</a>, <a href="https://github.com/sendhil">@sendhil</a>, <a href="https://github.com/SergioEstevao">@SergioEstevao</a>.</p>
<p>You can track the development progress for the next update by visiting <a href="https://github.com/wordpress-mobile/WordPress-iOS/issues?utf8=✓&q=is%3Aissue+milestone%3A6.5+" target="_blank">our 6.5 milestone on GitHub</a>. Until next time!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Aug 2016 12:27:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"diegoreymendez";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:28;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:50:"WP Mobile Apps: WordPress for Android: Version 5.7";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"http://apps.wordpress.com/?p=3535";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"https://apps.wordpress.com/2016/08/26/wordpress-for-android-version-5-7/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2330:"<p>Hello WordPress users! <a href="https://play.google.com/store/apps/details?id=org.wordpress.android" target="_blank">Version 5.7 of the WordPress for Android app</a> is now available in the Google Play Store.</p>
<h1>New &#8220;Plans&#8221; section in My Site</h1>
<p>Starting with 5.7, you can see your current WordPress.com plan and learn more about the benefits we offer in other plans.</p>
<p><img /></p>
<h1>Manage your followers and viewers from the &#8220;People Management&#8221; screen</h1>
<p>You&#8217;re now able to use the app to invite new Administrators, Editors, Authors or Contributors to your site, or remove unwanted followers.</p>
<p><img /></p>
<h1 id="other-changes">Other Changes</h1>
<p>Version 5.7 also comes with a few other changes and fixes:</p>
<ul>
<li>Reader tweaks in the Post Detail screen for tablets.</li>
<li>Keeps the &#8220;View Site&#8221; link visible for newly created users.</li>
<li>Fixes a rare crash when creating a new account.</li>
</ul>
<p>You can track our development progress for the next release by visiting <a href="https://github.com/wordpress-mobile/WordPress-Android/milestones/5.8">our 5.8 milestone on GitHub</a>.</p>
<h1>Beta</h1>
<p>Do you like keeping up with what’s new in the app? Do you enjoy testing new stuff before anyone else? Our testers have access to beta versions with updates shipped directly through Google Play. The beta versions may have new features, new fixes — and possibly new bugs! Testers make it possible for us to improve the overall app experience, and offer us invaluable development feedback.</p>
<p>Want to become a tester? <a href="https://play.google.com/apps/testing/org.wordpress.android">Opt-in</a>!</p>
<h1>Thank you</h1>
<p>Thanks to our GitHub contributors: <a href="https://github.com/0nko">@0nko</a>, <a href="https://github.com/aforcier">@aforcier</a>, <a href="https://github.com/hypest">@hypest</a>, <a href="https://github.com/karambir252">@karambir252</a>, <a href="https://github.com/khaykov">@khaykov</a>, <a href="https://github.com/kwonye">@kwonye</a>, <a href="https://github.com/maxme">@maxme</a>, <a href="https://github.com/mzorz">@mzorz</a>, <a href="https://github.com/nbradbury">@nbradbury</a>, <a href="https://github.com/oguzkocer">@oguzkocer</a>, and <a href="https://github.com/theck13">@theck13</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Aug 2016 11:33:19 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Maxime";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:29;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"Joseph: Trying Out The Twenty Sixteen Theme";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://josephscott.org/?p=15135";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:76:"https://blog.josephscott.org/2016/02/10/trying-out-the-twenty-sixteen-theme/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:547:"<p>Switched to the <a href="https://wordpress.org/themes/twentysixteen/">Twenty Sixteen WordPress Theme</a>.</p>
<p><a href="https://blog.josephscott.org/wp-content/uploads/2016/02/twentysixteen-screenshot-1.png" rel="attachment wp-att-15136"><img src="https://blog.josephscott.org/wp-content/uploads/2016/02/twentysixteen-screenshot-1-1024x768.png" alt="twentysixteen-screenshot" width="840" height="630" class="aligncenter size-large wp-image-15136" /></a></p>
<p>I opted to remove the sidebar, to give more focus on the content of the page.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 10 Feb 2016 18:11:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"josephscott";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:30;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:31:"Joseph: Recommended Consultants";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://josephscott.org/?p=9888";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://blog.josephscott.org/2015/07/22/recommended-consultants/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:277:"<p>My personal list of WordPress consultants has dried up ( some took full time jobs, the rest are always booked solid ).  Now I&#8217;m directing people to the <a href="http://jkudish.com/recommendations/">Recommended Consultants &amp; Resources list</a> from Joey Kudish.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 22 Jul 2015 14:01:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"josephscott";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:31;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:50:"Andrew Nacin: Smarter algorithms, smarter defaults";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:24:"http://nacin.com/?p=4329";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://nacin.com/2015/05/24/smart-algorithms-defaults/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:757:"<blockquote><p>Instead of showing the user an alert that something might not work, maybe we can build a smarter algorithm. Instead of asking the user to make a choice up front, maybe we can set a smart default and see if there is high demand after launch for more customization.</p></blockquote>
<p>— <a href="http://www.rebeccarolfe.com/">Rebecca Rolfe</a> on the Google Chrome team, interviewed in <a href="http://www.refinery29.com/female-white-hat-hackers-google-chrome">The Badass Women of Chrome&#8217;s Security Team</a> in Refinery29.</p>
<p>(More on making <a href="http://nacin.com/2011/12/18/in-open-source-learn-to-decide/">decisions</a>, <a href="http://nacin.com/2013/07/01/firefox-makes-a-decision-removes-an-option/">not options</a>.)</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 24 May 2015 23:11:28 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Andrew Nacin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:32;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:68:"Andrew Nacin: I’ve joined the White House’s U.S. Digital Service";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:24:"http://nacin.com/?p=4297";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:48:"https://nacin.com/2015/03/29/us-digital-service/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5753:"<blockquote><p>The need for effective government services is rising, while confidence in our ability to deliver them is dropping. More than ever, day-to-day interactions with government are powered by digital systems, and yet far too many Federal IT projects arrive late or over budget. Others are simply abandoned. These failures are often felt by those who count on it most — working class Americans and people who turn to government in a moment of need.<br />
<cite>The U.S. Digital Service on <a href="https://www.whitehouse.gov/digital/united-states-digital-service/story">whitehouse.gov</a></cite></p></blockquote>
<p>When you&#8217;re presented with an opportunity to help transform how the federal government works for the American people, it&#8217;s really hard to say no.</p>
<p>For five years and counting, I&#8217;ve had the honor and privilege as a lead developer of WordPress to play a role in a large, incredible movement to democratize publishing. From my home in D.C., I&#8217;ve closely watched <a href="http://www.data.gov/">open data</a> and <a href="https://m.whitehouse.gov/blog/2014/06/02/ostp-s-own-open-government-plan">open government efforts</a>. <a href="http://nacin.com/2014/03/24/an-hour-to-make-government-better/">I feel very strongly</a> about an open, transparent, and efficient government — boosted in no small part by WordPress and open source.</p>
<p>I&#8217;ve long admired a number of my new teammates, especially <a href="http://www.nextgov.com/cio-briefing/2015/03/meet-digital-service-guru-whos-helping-new-recruits-navigate-federal-bureaucracy/107148/">Erie Meyer</a>, <a href="http://apieconomist.com/blog/2013/4/10/6h6gntzp6twfuw4zwrzu0j43u0o80p">Gray Brooks</a>, and <a href="http://fcw.com/articles/2014/12/02/getting-started-at-usds.aspx">Haley van Dyck</a>, for years of tenacity and hard work trying to change government from the inside out. <span>I&#8217;ve always felt I could be more effective helping government from the outside, by continuing to work on WordPress. </span><span>After all, we&#8217;ve all heard horror </span><span>stories of all sorts of red tape, from hiring to procurement and everything in between. And we&#8217;ve all heard how difficult government itself makes it to launch good government digital services. While many of us may have have wanted to help, few thought they could. Fewer knew how.</span></p>
<p>But then the U.S. Digital Service was formed, from <a href="http://wired.com/2014/08/healthcare-gov/">the team that helped rescue healthcare.gov</a>. It&#8217;s dedicated to tackling some of government&#8217;s most pressing problems, ones that directly affect millions of people&#8217;s lives.<strong> </strong>The formula is simple: take what helped turn around healthcare.gov and <a href="http://playbook.cio.gov">apply it</a> to other high priority projects across government.</p>
<p>In this day and age, public policy must be backed by effective technology to succeed. The American people need our help and our government has asked us to serve, as <a href="https://www.usds.gov/">doers and makers, creative thinkers, and specialized technologists dedicated to untangling, rewiring, and redesigning our government</a>.</p>
<p>In January, I joined the U.S. Digital Service.</p>
<p>When I was approached, I have to admit that I was nervous to step back from the day-to-day buzz of WordPress because I&#8217;ve invested so much. But the community stepped up, in most cases not even knowing about my life change. That&#8217;s the beauty of open source, and the fantastic WordPress community in particular. WordPress continues to play an important role in my life. With <a href="http://ma.tt/">Matt Mullenweg</a>’s support and encouragement, I&#8217;m taking time away from Audrey, where I&#8217;ve worked since 2010. I&#8217;m still actively involved in the project, just not full time.</p>
<p>The U.S. Digital Service is the real deal. I&#8217;ve been astounded by the impact we&#8217;ve already made. We&#8217;ve recruited <a href="https://www.whitehouse.gov/blog/2015/03/19/president-obama-names-david-recordon-director-white-house-information-technology">some</a> <a href="http://www.washingtonpost.com/blogs/the-switch/wp/2014/09/04/white-house-names-googles-megan-smith-the-next-chief-technology-officer-of-the-united-states/">of</a> <a href="https://www.whitehouse.gov/blog/2014/08/20/day-one-mikey-dickerson-us-digital-service-administrator">the</a> <a href="https://www.whitehouse.gov/blog/2015/02/18/white-house-names-dr-dj-patil-first-us-chief-data-scientist">best</a> <a href="https://www.whitehouse.gov/blog/2015/02/05/next-us-chief-information-officer">and</a> <a href="https://medium.com/@USDigitalService/why-i-joined-the-u-s-digital-service-24c6682afce2?source=latest">brightest</a>. Don&#8217;t just take my word for it — do what you can to <a href="https://medium.com/@USDigitalService/mikey-dickerson-to-sxsw-why-we-need-you-in-government-f31dab3263a0">learn more about this movement</a> and <a href="http://www.usds.gov/">come help us make government better</a>. If you haven&#8217;t seen the video below yet, take a look. (A few of you have noticed me in the background.)</p>
<p>It&#8217;s my nature to look for the hardest problems to solve. I like to take on big challenges and spend every ounce of energy working to solve them. I believe in what we&#8217;re doing here. The stakes are high. No matter the challenge, I know what we&#8217;re doing will change millions of lives.</p>
<p>I thought I had made the most of my decade in D.C. I&#8217;ve witnessed a lot of history. I knew I&#8217;d have some great stories to tell my future kids and grandkids. <em>I was there. I saw it.</em> That was only the beginning.</p>
<p></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 29 Mar 2015 17:25:02 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Andrew Nacin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:33;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:28:"Ping-O-Matic: A Prompt a Day";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"http://blog.pingomatic.com/?p=115";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://blog.pingomatic.com/2015/01/13/a-prompt-a-day/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:863:"<p><a href="http://pingomatic.com/" target="_blank">Ping-O-Matic</a> is all about getting your latest content out into the world and driving more traffic to your blog. Sometimes, though, we need inspiration to write.</p>
<p>Our friends at <em><a href="http://dailypost.wordpress.com" target="_blank">The Daily Post</a></em> published a free ebook of daily prompts: a gentle nudge to encourage a regular blogging habit. It&#8217;s available in four languages: <strong>English</strong>, <strong>French</strong>, <strong>Spanish</strong>, and <strong>Indonesian</strong>:</p>
<p><a href="https://dailypost.wordpress.com/postaday/ebook-365-writing-prompts/" target="_blank">Ebook: 365 Writing Prompts</a></p>
<p><a href="https://dailypost.wordpress.com/postaday/ebook-365-writing-prompts/"><img /></a></p>
<p>Download the version you need &#8212; it&#8217;s free!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 13 Jan 2015 18:16:46 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:20:"Cheri Lucas Rowlands";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:34;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"Gravatar: Something funny happened";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"http://blog.gravatar.com/?p=511";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"https://blog.gravatar.com/2014/07/23/something-funny-happened/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:723:"<p>On the way to building a <a href="https://blog.gravatar.com/2014/03/05/a-question-for-gravatar-users/">Gravatar app</a>, we noticed that taking pictures of ourselves to update our Gravatars was something we only wanted to do every month or so, but then we started taking selfies and sharing them with each other and that became a daily and very fun habit. So our Gravatar app morphed into a <a href="http://getselfies.com/">Selfies app</a>, and it&#8217;s now ready for the world to play with! You can read more <a href="http://getselfies.com/about/">about the app</a> here. We hope you become one of the first brave souls to try it out, and let us know <a href="http://feedback.getselfies.com/">what you think</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 23 Jul 2014 18:41:31 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Toni";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:35;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"Ping-O-Matic: Tips on Growth From a Book Blogger";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"http://blog.pingomatic.com/?p=107";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://blog.pingomatic.com/2014/06/12/book-blogger-tips/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4043:"<p><a href="http://101books.net/"><img /></a><br />
Over on <a href="https://dailypost.wordpress.com/" target="_blank"><em>The Daily Post</em></a>, we’ve published interviews with writers on growth and readership. Robert Bruce, the blogger behind the popular book site, <a href="http://101books.net/" target="_blank"><em>101 Books</em></a>, shared bits of advice on building a loyal readership and successful niche blog. Here are snippets from his Q&amp;A:</p>
<h3>You&#8217;ve got over 26,000 followers and counting. What&#8217;s your secret?</h3>
<p>There’s really no secret. It’s just steady, consistent posting over a long period of time. <em>101 Books</em> is more than three years old now, and I’ve had more than 700 posts. When you post that often, people are bound to find you. Then, the key is to just write content that relates to them. Most people don’t care about what you had for breakfast, but if you can help them learn something new, then they’ll keep coming back.</p>
<h3>What types of posts perform better?</h3>
<p>The funny thing about my blog is that, even though it’s centered on <a href="http://101books.net/faq/" target="_blank">the “101 Books” project</a>, these book reviews don’t perform as well as the quirkier stuff. One of the most popular posts I’ve had was a post about <a href="http://101books.net/2013/04/26/my-2-year-old-judges-books-by-their-covers/" target="_blank">my two-year-old son judging books by their covers</a>. I put a couple of classic book covers in front of him and asked him what he thought they were about. His answers were hilarious. That post took about 15 minutes to put together, but because it was unique and fresh, it became a hit.</p>
<p>Obviously, list-style posts do well, and I probably tend to overuse them because of that. (I’m not BuzzFeed.) Also, for whatever reason, people gravitate to more negative-sounding titles, like <a href="http://101books.net/?s=horrible+death" target="_blank">“7 Words That Should Die A Horrible Death.”</a></p>
<h3>There are many blogs about books on the web. Why do you think yours has been so successful?</h3>
<blockquote><p>I think people can easily get behind the idea of someone pursuing a crazy goal and the ups and downs that come with that.</p></blockquote>
<p>There’s a lot of great book blogs out there, and a lot of bloggers who write incredibly detailed book reviews. My blog is a little different because I review books in small chunks; I take a small passage from a book and write about it. Or I write about some cool, unusual fact from the author’s background. So I think it stands out a bit in the book blogging world.</p>
<p>Plus, I think people can easily get behind the idea of someone pursuing a crazy goal and the ups and downs that come with that. It’s like a literary version of the <em>Julie and Julia</em> book (and movie). Not that I’m near as creative and successful as she was, but you get the point.</p>
<h3>Do you have tips for someone who wants to focus their blog on books?</h3>
<blockquote><p>Sometimes you’ve just got to push the publish button because an almost-perfect blog post is better than no post at all.</p></blockquote>
<p>Be honest. Don’t feel like you have to like a book or dislike a book because of what the critics say. On my blog, I’m very vocal of my dislike for <em>Mrs. Dalloway</em>, but it’s my honest opinion.</p>
<p>If you want to write a book blog with an academic voice, that’s great. But you’ll probably realize that not many people will read it. I try to write about literature in an approachable way, and that style involves <em>forgetting</em> what my English literature professor taught me.</p>
<p>I think it’s also important to <a href="http://101books.net/2013/04/19/3-tips-for-a-better-blog/" target="_blank">forget about being perfect</a>. Sometimes you’ve just got to push the publish button because an almost-perfect blog post is better than no post at all. Don’t pass over the great in search of the perfect.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 12 Jun 2014 16:00:25 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:20:"Cheri Lucas Rowlands";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:36;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"Mike Little: Apologies for the old posts.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"https://journalized.zed1.com/?p=2023";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:77:"https://journalized.zed1.com/archives/2014/04/22/apologies-for-the-old-posts/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:600:"<p>Folks, it looks like when I moved this old blog from a subdirectory to a subdomain, <a href="http://planet.wordpress.org">planet.wordpress.org</a> (the feed that shows up in your dashboard) thought my last few old posts were new. Hence a lot of old stuff appeared in your dashboard.</p>
<p>Sorry for the confusion&#8230;</p>
<p>Mike</p>
<p>The post <a rel="nofollow" href="https://journalized.zed1.com/archives/2014/04/22/apologies-for-the-old-posts/">Apologies for the old posts.</a> appeared first on <a rel="nofollow" href="https://journalized.zed1.com">Mike Little&#039;s Journalized</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 22 Apr 2014 14:36:24 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Mike Little";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:37;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:31:"Andrew: Customizing TinyMCE 4.0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"http://azaozz.wordpress.com/?p=380";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"https://ozz.blog/2014/04/19/customizing-tinymce-4-0/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1998:"<p>Many of the TinyMCE settings have changed in version 4.0. There is a new default theme: Modern, and all the UI settings for the former Advanced theme (<code>theme_advanced...)</code> are deprecated.</p>
<p>One often used setting was <code>theme_advanced_blockformats</code><strong><code>.</code></strong> It was renamed to <code>block_formats</code> and keeps the same formatting. To specify a different set of elements for the &#8216;blockformats&#8217; drop-down (second toolbar row in the WordPress Visual editor), you can set a string of name=value pairs separated by a semicolon in the initialization object:</p>
<pre class="brush: jscript; title: ; notranslate">block_formats: "Paragraph=p;Heading 1=h1;Heading 2=h2;Heading 3=h3"</pre>
<p>Another handy setting: <code>theme_advanced_styles</code> doesn&#8217;t exist any more. However there is a more powerful version: <code>style_formats</code>. Now it can replace or add items to the new &#8220;Formats&#8221; menu.The value is an array of objects each containing a name that is displayed as sub-menu and several settings: a CSS class name or an inline style, and optionally the wrapper element where the class or inline style will be set:</p>
<pre class="brush: jscript; title: ; notranslate">
toolbar3: 'styleselect',
style_formats_merge: true,
style_formats: { name: 'Custom styles', [
  {title: 'Red bold text', inline: 'b', styles: {color: '#ff0000'}},
  {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
  {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
  {title: 'Example 1', inline: 'span', classes: 'example1'},
  {title: 'Example 2', inline: 'span', classes: 'example2'}
]}
</pre>
<p>The above code will add another sub-menu to &#8220;Formats&#8221; without replacing the default menu items. There is <a href="http://www.tinymce.com/wiki.php/Configuration:style_formats">more information</a> and an <a href="http://www.tinymce.com/tryit/custom_formats.php">example</a> on the TinyMCE website.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 19 Apr 2014 07:04:36 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Andrew Ozz";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:38;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"Gravatar: Mobile Friendly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"http://blog.gravatar.com/?p=503";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://blog.gravatar.com/2014/03/13/mobile-friendly/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:177:"<p>We just made the Gravatar.com website, blog, and all profile pages friendlier towards mobile devices. Additionally, you can now edit your profile on the go.</p>
<p>Enjoy!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 13 Mar 2014 08:52:32 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"Joen A.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:39;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"Lloyd: One Year and Ten Months Without Pants";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:28:"http://foolswisdom.com/?p=84";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:58:"https://foolswisdom.com/2013/09/23/1-and-10-without-pants/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:8466:"<p>I finished reading <a href="http://www.amazon.com/gp/product/1118660633/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=1118660633&linkCode=as2&tag=foolswisdomco-20">The Year Without Pants: WordPress.com and the Future of Work by <strong>Scott Berkun</strong></a> Thursday night. It&#8217;s already one of my favorite books ever, but I&#8217;m obviously incredibly biased here!</p>
<p>It&#8217;s weird to read a book covering a big chunk of your own life. I was employee 10 at <a href="http://automattic.com/">Automattic</a> and worked there for five of the best years of my life. I was there for most of the time that Berkun was, though it felt like we were at opposites ends of the company. I left Automattic about six months before Berkun left. I wish I had this book to read while I was at Automattic. It would have <strong>shaped my thinking</strong>!</p>
<p>Of course, this is going to be my favorite Berkun book, but there is a huge awkwardness of it all &#8212; are there other business leadership books of this style? has anyone pulled off keeping themselves in it? Berkun comes off as machiavellian, when I don&#8217;t think he really is.</p>
<p>There is a real reason why all autobiographies are incomplete, and great biographies either burn relationships or are written about the finished and the dead.</p>
<p>I&#8217;m eager to hear and read <a href="http://automattic.com/about/">Automatticians,</a> &#8220;formermatticians&#8221;, and other insiders thoughts on the book, particularly expanding on or challenging the history and stories of Automattic that Berkun shares. Don&#8217;t get me wrong, the insights that can be applied regardless of how Scott&#8217;s experiences and perspective align. The book is fully of incredible project management and business leadership insights that will stand on their own through the tests of time.</p>
<p>It&#8217;s only been a week since the release, but I haven&#8217;t seen any reviews that really expand on the history of Automattic.</p>
<p>I do really enjoy how mdawaffe, aka Mike Adams, ends his post &#8220;<a href="http://mdawaffe.wordpress.com/2013/09/20/seven-years-without-pants/" rel="bookmark">Seven Years Without Pants</a>&#8221; with:</p>
<blockquote><p>I’m pretty sure the party was only the eighth time Scott and I had ever met in meatspace (a.k.a. “real life”). A good friend, my former boss, and a formative teacher of mine, Scott and I have only ever seen each other eight times. If you don’t understand how that’s possible, read <a href="http://www.amazon.com/exec/obidos/ASIN/1118660633/foolswisdomco-20/">the book</a>.</p></blockquote>
<p>I&#8217;ll update this article with online discussions from insiders as I find them.</p>
<p><strong>Interesting</strong></p>
<ul>
<li><a href="http://evansolomon.me/">Evan Solomon</a> in &#8220;<a href="https://medium.com/lessons-learned/8076216a30b9">A Two Year Old Conversation Shows Up In a Boo</a>k&#8221;<br />
<blockquote><p>I think [letting someone be employee and reporter at the same time is a] mistake. It puts employees in a difficult and ultimately lose-lose situation to be unsure whether conversations are private or public. It doesn’t bother me much that Scott was frustrated by my communication style, but I don’t think it’s his place to share that frustration with the world any more than it would be my place to talk about my current colleague’s personal habits at work without their permission. I would consider doing that a clear violation of trust.</p></blockquote>
</li>
</ul>
<p><strong>Storied<br />
</strong></p>
<ul>
<li>none so far</li>
</ul>
<p><strong>Soft</strong></p>
<ol>
<li><a href="http://raanan.com/2013/09/17/a-book-wordpress-com-the-year-without-pants/">Raanan Bar-Cohen</a><br />
<blockquote><p>&#8220;A bit surreal to read about your own work, and I’ve found over the years that all my colleagues have a great work ethic.</p>
<p>What I like is that Scott hits on a point that I find very true — which is that companies that have big audacious goals such as ours, and give employees freedom to define the methods of achieving them – tend to attract people who are passionate and love what they do. And that combo tends to result in amazing outcomes and companies that have a culture that attracts fantastic talent.&#8221;</p></blockquote>
</li>
<li><a href="http://dentedreality.com.au/2013/09/17/the-year-without-pants/">Beau Lebens</a></li>
<li><a href="http://apeatling.wordpress.com/2013/09/17/the-year-without-pants/">Andy Peatling</a></li>
<li><a href="http://en.blog.wordpress.com/2013/09/17/scott-berkun-interview/">WordPress.com Interview</a> by Krista Stevens<br />
<blockquote><p><strong>Berkun</strong>: I was most surprised to rediscover that it’s the fundamentals. If you can build trust, provide clarity, and hire well, every other obstacle can be conquered. My story in The Year Without Pants follows how I tried to achieve those things despite a decade age gap, 100% remote workers, radically different culture, and more, any of which would be terrifying to most managers on their own, including myself.</p></blockquote>
</li>
<li><a href="http://alexking.org/blog/2013/09/17/the-year-without-pants">Alex King</a><br />
<blockquote><p>I found Scott’s story to be interesting and self-aware; an introspective and honest account of how his team operated. I am fortunate to be friends (or at least friendly) with many of the people mentioned in the book. Scott’s characterizations of them, their personalities and humor struck me due to their accuracy with my own experiences. This made it easy for me to fully embrace what I was reading.</p></blockquote>
</li>
<li><a href="http://paulmaiorana.com/notes/2013/09/the-year-without-pants/">Paul Maiorana</a></li>
<li><a href="http://justhugo.com/2013/09/17/the-year-without-pants/">Hugo Baeta</a></li>
<li><a href="https://www.goodreads.com/review/show/727264856">Lance Willett</a></li>
</ol>
<p>Matt mentioned &#8220;This news comes in a fun week generally: <a href="http://www.amazon.com/dp/1118660633/foolswisdomco-20/">Scott Berkun’s book about Automattic is out today</a> and getting rave reviews&#8221; in <a href="http://ma.tt/2013/09/more-tiger-secondary/">More Tiger Secondary</a>.</p>
<p><strong>Hoping</strong></p>
<ul>
<li><a href="http://tonyconrad.wordpress.com/">Tony Conrad</a></li>
<li><a href="http://toni.org/">Toni Schneider</a></li>
<li><a href="http://terrychay.com/">Terry Chay</a></li>
<li><a href="http://boren.nu/">Ryan Boren</a></li>
<li><a href="https://twitter.com/MrVelvet">Phil Black</a></li>
<li><a href="https://medium.com/@pdavies">Pete Davies</a></li>
<li><a href="http://blog.numenity.org/">Paul Kim</a></li>
<li><a href="http://om.co/">Om Malik</a></li>
<li><a href="https://twitter.com/nonotate">Noriyko Tate </a></li>
<li><a href="http://noeljackson.com/">Noël Jackson</a></li>
<li><a href="http://nickmomrik.com/">Nick Momrik</a></li>
<li><a href="http://www.niallkennedy.com/">Niall Kennedy</a></li>
<li><a href="http://vcmike.wordpress.com/">Mike Hirshland</a></li>
<li><a href="https://twitter.com/madebypick">Michael Pick</a></li>
<li><a href="http://mayadesai.wordpress.com/">Maya Desai</a></li>
<li><a href="http://mattnt.com/">Matt Thomas</a></li>
<li><a href="http://ma.tt/">Matt Mullenweg</a></li>
<li><a href="http://romanticrobot.net/">Mark Riley</a></li>
<li><a href="http://markjaquith.com/">Mark Jaquith</a></li>
<li><a href="http://weblogtoolscollection.com/">Mark Ghosh</a></li>
<li><a href="http://loriloo.com/">Lori McLeese </a></li>
<li><a href="http://lorelle.wordpress.com/">Lorelle VanFossen</a></li>
<li><a href="http://zxcode.com/">Lenny Lenehan</a></li>
<li><a href="https://josephscott.org/">Joseph Scott</a></li>
<li><a href="http://jaco.by/">John James Jacoby</a></li>
<li><a href="http://jenmylo.com/">Jen Mylo (Jane Wells)</a></li>
<li><a href="http://ocaoimh.ie/">Donncha O Caoimh</a></li>
<li><a href="https://foolswisdom.com/tag/wordpress/feed/www.apokalyptik.com/‎">Demitrious Kelly</a></li>
<li><a href="http://darylkoop.com/">Daryl Koopersmith</a></li>
<li><a href="http://chexee.me/">Chelsea Otakan</a></li>
<li><a href="http://barry.wordpress.com/">Barry Abrahamson</a></li>
<li><a href="http://www.dorman.com/">Anne Dorman</a></li>
<li><a href="http://andy.wordpress.com/">Andy Skelton</a></li>
</ul>
<p>What topics around Automattic and the WordPress.com business are you most interested in learning more about?</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Sep 2013 16:17:17 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Lloyd Dewolf";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:40;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"Peter Westwood: SSL all the things";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"http://westi.wordpress.com/?p=6394";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:58:"https://westi.wordpress.com/2013/08/07/ssl-all-the-things/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1048:"<p>Security is important and one of the things I would like to see is if we can enforce a requirement for all requests that core makes back to WordPress.org for updates and information to be https. This is the a great step to a greater level of update verification to help detect man-in-the-middle attacks.</p>
<p>Making this switch is going to be a fun journey and we are bound to find that there are some setups that can&#8217;t/don&#8217;t/won&#8217;t support https with the WP_HTTP API.</p>
<p>So before we try switching to using https in trunk I&#8217;ve update the <a href="http://wordpress.org/plugins/wordpress-beta-tester/">Beta Tester plugin</a> so that it forces all requests to api.wordpress.org to happen over https. I&#8217;ve also updated the api so that if you make a https request it will return https references in the results.</p>
<p>Please go for and test this on your test installs and let us know of any issues you find here in the comments or <a href="http://core.trac.wordpress.org/ticket/18577">on the trac ticket</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 07 Aug 2013 13:25:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Peter Westwood";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:41;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"Peter Westwood: Blog rebuilding …";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"http://westi.wordpress.com/?p=6388";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://westi.wordpress.com/2013/05/27/blog-rebuilding/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:477:"<p>It&#8217;s been a long time since I&#8217;ve posted here and I feel like today is the special day when I should start trying to rebuild this blog, adding more content and giving it a lick of new paint.</p>
<p>So I&#8217;ve switched to Twenty Thirteen, and here is the first short post of hopefully more to come, for more about <a href="http://blog.ftwr.co.uk/archives/2013/05/27/a-decade-gone-more-to-come/">my WordPress journey so far head over to my personal blog</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 May 2013 17:52:53 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Peter Westwood";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:42;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:53:"Mike Little: WordPress 10th Anniversary: a Reflection";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"https://journalized.zed1.com/?p=1929";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:89:"https://journalized.zed1.com/archives/2013/05/27/wordpress-10th-anniversary-a-reflection/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:470:"<p>I posted about <a href="http://mikelittle.org/wordpress-10th-anniversary/">WordPress&#8217; 10th Anniversary</a> celebration and reflected on the last 10 years over on my new blog</p>
<p>The post <a rel="nofollow" href="https://journalized.zed1.com/archives/2013/05/27/wordpress-10th-anniversary-a-reflection/">WordPress 10th Anniversary: a Reflection</a> appeared first on <a rel="nofollow" href="https://journalized.zed1.com">Mike Little&#039;s Journalized</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 27 May 2013 14:31:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Mike Little";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:43;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"mdawaffe: WordCamp OC: REST/JSON API Slides";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://mdawaffe.wordpress.com/?p=262";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:74:"https://mdawaffe.wordpress.com/2012/06/13/wordcamp-oc-restjson-api-slides/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:372:"<p><img class="alignright" src="https://i2.wp.com/2012.oc.wordcamp.org/files/2012/04/wcoc2012_im_speaking.png" /></p>
<p>Here&#8217;s the slides for my <a href="http://2012.oc.wordcamp.org/">WordCamp OC 2012</a> presentation.</p>
<p>My <a href="https://mdawaffe.wordpress.com/2012/06/02/wordcamp-oc-restjson-api-talk/">previous post</a> has several links and examples.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 13 Jun 2012 06:44:38 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Mike Adams";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:44;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"mdawaffe: WordCamp OC: REST/JSON API Talk";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://mdawaffe.wordpress.com/?p=247";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"https://mdawaffe.wordpress.com/2012/06/02/wordcamp-oc-restjson-api-talk/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:646:"<p><img class="alignright" src="https://i2.wp.com/2012.oc.wordcamp.org/files/2012/04/wcoc2012_im_speaking.png" /></p>
<p>I&#8217;m speaking at <a href="http://2012.oc.wordcamp.org/">WordCamp Orange County 2012</a> today about WordPress&#8217; core HTTP APIs and WordPress.com&#8217;s <a href="http://developer.wordpress.com/docs/api/">REST API</a>.</p>
<p>This post has a bunch of links and examples for the attendees to reference later.</p>
<p>Click the &#8220;Pages&#8221; links below to find everything.</p>
<p><strong>Update</strong>: <a href="https://mdawaffe.wordpress.com/2012/06/13/wordcamp-oc-restjson-api-slides/">Slides are up</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 02 Jun 2012 22:33:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Mike Adams";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:45;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"Andrew: Server managed cache in the browser";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"http://azaozz.wordpress.com/?p=145";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://ozz.blog/2011/05/11/server-managed-cache-in-the-browser/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5663:"<p>Imagine browsing to a big web page with lots of images and scripts, and it loads in your browser almost instantly, nearly as fast as loading it from your hard drive. Now imagine you&#8217;re browsing a web site with about 60-70 of these pages and they all load very very fast. Sounds interesting? But how to do that? Prime the browser&#8217;s cache? Preload all components of the web pages somehow? Is that possible?</p>
<p>Well, yes and no. It is possible by using <a href="http://gears.google.com/">Gears</a>. It can be set to store all &#8220;static&#8221; components (JS, CSS, images, etc.) of a web page or a whole web site and load them from the local storage every time they are requested by the browser. However the Gears team <a href="http://gearsblog.blogspot.com/2010/02/hello-html5.html">shifted their priorities</a> to HTML 5.0 offline storage which was the main idea behind Gears in the first place. Unfortunately the HTML 5.0 specification for offline storage implements only some of the features that were available in Gears, so this type of caching (controlled by the user and managed by the server) is impossible.</p>
<p><em>But why server managed cache? Isn&#8217;t the standard browser caching good enough?</em> Yes, it is good. It has evolved significantly during the 15 or so years since the beginning of the World Wide Web. However it just can&#8217;t do that.</p>
<p>Lets take a simplistic look at how the browser cache works:</p>
<ul>
<li>We (the users) browse to a web page.</li>
<li>The Server tells the Browser: &#8220;Hey there, these few files (images, JS, CSS, etc.) are almost never updated, put them in your cache and don&#8217;t ask me to send them again for the next 10 years.&#8221;</li>
<li>The Browser thinks: &#8220;Hmm, put them in the cache you saying? I&#8217;ll think about it. You know, I&#8217;m a Web Browser. I need to load pages very very fast. I don&#8217;t want a huge cache with millions of files in it. That will slow me down. Lets see if the User would come back to this page ever again.&#8221;</li>
</ul>
<p>If we keep going to the same web page eventually the Browser would change his mind: &#8220;Maybe that Server was right and I should put these files in my cache. That would speed up page loading. But what will happen if these files are updated&#8230; I better keep asking the Server to check if they have been updated so my cache is always fresh.&#8221;</p>
<p>Couple of years ago we implemented Gears as WordPress&#8217; Turbo feature. We didn&#8217;t use it to make WordPress an offline app, we used it to create server managed cache. It worked great. Even the heaviest pages in the WordPress admin were loading considerably faster regardless of how often the users were visiting them.</p>
<p>The implementation was very simple: we had a manifest that listed all &#8220;static&#8221; files and couple of user options to enable and initialize the &#8220;super cache&#8221;. The rest was handled automatically by Gears. So in reality we discovered the perfect way of browser caching for web apps:</p>
<ul>
<li>The User decides which web sites / web apps are cached and can add or remove them.</li>
<li>The server (i.e. the web app) maintains the cache, updating, adding, deleting files as needed.</li>
</ul>
<p>The results were spectacular. We didn&#8217;t need to concatenate and compress scripts and stylesheets. We even stopped compressing TinyMCE which alone can load about 30-40 files on initialization. And page load time was from 0.5 to 1.5 sec. no matter how heavy the page was. For comparison before implementing this &#8220;super caching&#8221; pages were loading in 5 to 9 sec.</p>
<p><em>Why was it performing that well?</em> Simple: it eliminated all requests to the server for the files that were cached. And that means <strong>all</strong>, even the &#8220;HEAD&#8221; requests. In our implementation the only file that was loaded from the server was the actual HTML. All other components of the web page were stored in Gears&#8217; offline storage.</p>
<p>That also had the side benefit of eliminating a big chunk of traffic to the server. At first look it doesn&#8217;t seem like a lot, 30-40 requests for the web page components followed by 30-40 of HEAD requests per page every now and then (while the browser cache is hot), but think about it in global scope: several millions of these pages are loaded every hour.</p>
<p><em>So, why not do the same with HTML 5.0 offline storage?</em> Because it doesn&#8217;t work that way. The HTML 5.0 specification for offline storage is good only for&#8230; Offline storage. It&#8217;s missing a lot of the features Gears has. Yes, there is a workaround. We can &#8220;store offline&#8221; a skeleton of the web page and then load all the dynamic content with XHR (a.k.a. AJAX), but that method has other (quite annoying) limitations. Despite that we will try this method in WordPress for sure, but that discussion is for another post.</p>
<p><em>In short:</em> the HTML 5.0 offline storage implementation is missing some critical features. For example a file that is stored there <strong>is not</strong> loaded from the storage when the browser goes to another page on the same website. Yes, it&#8217;s sad watching the browser load the same file again and again from the Internet when that file is already on the user&#8217;s hard drive.</p>
<p><em>What can we do about it?</em> Don&#8217;t think there is anything that can be done short of changing, or rather enhancing the HTML 5.0 specification for offline storage. The XHR &#8220;hack&#8221; that makes this kind of caching possible with the current HTML 5.0 is still just a hack.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 11 May 2011 15:32:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Andrew Ozz";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:46;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"Andy Skelton: SxSW Bonfire Tues March 16";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:64:"http://andy.wordpress.com/2010/03/08/sxsw-bonfire-tues-march-16/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:65:"https://andy.wordpress.com/2010/03/08/sxsw-bonfire-tues-march-16/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:502:"<p>It&#8217;s settled. There will be a party at my house in Dripping Springs the afternoon/evening of Tuesday, March 16, to mark the end of the Interactive festival.</p>
<p>There will be some shuttle transportation to and from downtown Austin. We will share what refreshments we have. Please bring anything you would like to drink or cook around a fire. </p>
<p>There will be no high-speed internet. Come early to work the land, play with the dogs, or just sit in the shade of an enormous oak tree.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 08 Mar 2010 15:15:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Andy Skelton";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:" * data";a:8:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Thu, 07 Nov 2019 10:28:00 GMT";s:12:"content-type";s:8:"text/xml";s:4:"vary";s:15:"Accept-Encoding";s:13:"last-modified";s:29:"Thu, 07 Nov 2019 10:15:07 GMT";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:9:"HIT ord 1";s:16:"content-encoding";s:2:"br";}}s:5:"build";s:14:"20190927180038";}";}